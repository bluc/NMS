#!/usr/bin/env bash
#--------------------------------------------------------------------
# (c) CopyRight 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

if [ ! "T$EUID" = 'T0' ]
then
    echo "Only 'root' can run this script"
    exit 99
fi

#-------------------------------------------------------------------------
# Remove temporray files at the end
trap "rm -f /tmp/$$*" EXIT

#-------------------------------------------------------------------------
LOCAL_OS=$(uname -s)
case $LOCAL_OS in
Linux|SunOS)
    ;;
*)  echo "Unsupported operating system '$LOCAL_OS'"
    exit 1
    ;;
esac

#-------------------------------------------------------------------------
# Setup the nms account if its doesn't exist yet
getent passwd nms &> /dev/null
if [ $? -ne 0 ]
then
    useradd -s $BASH -c 'NMS Monitor' -m nms
else
    usermod -s $BASH -c 'NMS Monitor' nms
fi

#-------------------------------------------------------------------------
# Update/set the password
if [ "T$LOCAL_OS" = 'TLinux' ]
then
    echo 'nms:b3kkajudd' | chpasswd
    # Allow account to read logs
    usermod -G adm nms
elif [ "T$LOCAL_OS" = 'TSunOS' ]
then
    echo "When asked, please set the password for 'nms' to 'b3kkajudd'"
    sleep 2
    passwd nms
fi

#-------------------------------------------------------------------------
# Create the necessary sudo definitions
if [ -d /etc/sudoers.d ]
then
    # Linux host
    cat << EOT > /etc/sudoers.d/NMS
nms ALL= NOPASSWD: /usr/sbin/dmidecode
EOT
    chmod 0440 /etc/sudoers.d/NMS
elif [ -d /usr/local/etc/sudoers.d ]
then
    # Solaris 10 host
    cat << EOT > /usr/local/etc/sudoers.d/NMS
nms ALL= NOPASSWD: /usr/sbin/fmadm
EOT
    chmod 0440 /usr/local/etc/sudoers.d/NMS
elif [ -s /usr/local/etc/sudoers ]
then
    # Solaris 8 host
    if [ -z "$(grep ^nms /usr/local/etc/sudoers)" ]
    then
        cat << EOT >> /usr/local/etc/sudoers
nms ALL= NOPASSWD: /usr/sbin/fmadm
EOT
    fi
else
    cat << EOT
Is 'sudo' even installed on this host?

If yes, please invoke 'EDITOR=vi visudo' and add these
lines to the file:
EOT
    if [ "T$LOCAL_OS" = 'TLinux' ]
    then
        echo 'nms ALL= NOPASSWD: /usr/sbin/dmidecode'
    elif [ "T$LOCAL_OS" = 'TSunOS' ]
    then
        echo 'nms ALL= NOPASSWD: /usr/sbin/fmadm'
    fi
fi

#-------------------------------------------------------------------------
# Some parting words
cat  << EOT

To fully activate this host, please run these commands
  as "root" on the NMS server:

ssh-copy-id -i /var/lib/nagios/.ssh/id_nms nms@<host-ip>
sudo -u nagios ssh-copy-id -i /var/lib/nagios/.ssh/id_nms nms@<host_ip>

Also create the necessary host definition in /etc/icinga2/conf.d
on the NMS server.

This host's IP addresses are:
EOT
ifconfig -a
sleep 2

#-------------------------------------------------------------------------
# We are done
exit 0
