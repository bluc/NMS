#!/bin/bash
logger -it icinga2-mail -- $0 $@

# ADAPT THESE to your environment:
DEFAULT_EMAIL_RECIPIENT='consult@btoy1.net'
DEFAULT_EMAIL_SENDER='consult@btoy1.net'
DEFAULT_WEBURL="http://localhost/icingaweb2"

## Function helpers
Usage() {
cat << EOF

Required parameters:
  -4 HOSTADDRESS (address)
  -d LONGDATETIME (icinga.long_date_time)
  -l HOSTNAME (host.name)
  -n HOSTDISPLAYNAME (host.display_name)
  -o HOSTOUTPUT (host.output)
  -r USEREMAIL (user.email)
  -s HOSTSTATE (host.state)
  -t NOTIFICATIONTYPE (notification.type)

Optional parameters:
  -6 HOSTADDRESS6 (address6)
  -b NOTIFICATIONAUTHORNAME (\notification.author)
  -c NOTIFICATIONCOMMENT (notification.comment)
  -i ICINGAWEB2URL (notification_icingaweb2url, Default: unset)
  -f MAILFROM (notification_mailfrom, requires GNU mailutils (Debian/Ubuntu) or mailx (RHEL/SUSE))
  -v (notification_sendtosyslog, Default: false)

EOF
}

Help() {
  Usage;
  exit 0;
}

Error() {
  if [ "$1" ]; then
    echo $1
  fi
  Usage;
  exit 1;
}

## Main
while getopts 4:6::b:c:d:f:hi:l:n:o:r:s:t:v: opt
do
  case "$opt" in
    4) HOSTADDRESS=$OPTARG ;; # required
    6) HOSTADDRESS6=$OPTARG ;;
    b) NOTIFICATIONAUTHORNAME=$OPTARG ;;
    c) NOTIFICATIONCOMMENT=$OPTARG ;;
    d) LONGDATETIME=$OPTARG ;; # required
    f) MAILFROM=$OPTARG ;;
    h) Help ;;
    i) ICINGAWEB2URL=$OPTARG ;;
    l) HOSTNAME=$OPTARG ;; # required
    n) HOSTDISPLAYNAME=$OPTARG ;; # required
    o) HOSTOUTPUT=$OPTARG ;; # required
    r) USEREMAIL=$OPTARG ;; # required
    s) HOSTSTATE=$OPTARG ;; # required
    t) NOTIFICATIONTYPE=$OPTARG ;; # required
    v) VERBOSE=$OPTARG ;;
   \?) echo "ERROR: Invalid option -$OPTARG" >&2
       Error ;;
    :) echo "Missing option argument for -$OPTARG" >&2
       Error ;;
    *) echo "Unimplemented option: -$OPTARG" >&2
       Error ;;
  esac
done

shift $((OPTIND - 1))

# Some defaults for possibly missing parameters
[ ! "$USEREMAIL" ] && USEREMAIL="${DEFAULT_EMAIL_RECIPIENT}"

## Check required parameters (TODO: better error message)
## Keep formatting in sync with mail-service-notification.sh
for P in HOSTADDRESS LONGDATETIME HOSTNAME HOSTDISPLAYNAME HOSTOUTPUT HOSTSTATE USEREMAIL NOTIFICATIONTYPE
do
    eval "PAR=\$${P}"
    if [ ! "$PAR" ]
    then
        Error "Parameter '$P' is missing"
    fi
done

# ADAPT this:
[ -z "$ICINGAWEB2URL" ] && ICINGAWEB2URL="${DEFAULT_WEBURL}"
[ -z "$MAILFROM" ] && MAILFROM="${DEFAULT_EMAIL_SENDER}"

PRIO_HEADERS=''
case ${HOSTSTATE^^} in
DOWN)
    PRIO_HEADERS='-aX-Priority:1 -aPriority:High -aX-MSMail-Priority:High -aImportance:High'
    ;;
UP)
    PRIO_HEADERS='-aX-Priority:2'
    ;;
esac
NMS_ID=$(date +NMS-%FT%T-%N'-'$RANDOM)
for UE in $(awk '{gsub(/,/," ");print}' <<< $USEREMAIL)
do
    if [ -z $(grep -P '^\d{10}' <<< $UE) ]
    then
        # Standard email
        mail -s "$NOTIFICATIONTYPE - $HOSTDISPLAYNAME is $HOSTSTATE" \
          -aFrom:$MAILFROM -aReply-To:${DEFAULT_EMAIL_SENDER} \
          $PRIO_HEADERS $UE <<TEMPLATE
***** Network Monitoring System  *****

Notification Type: $NOTIFICATIONTYPE
 $HOSTOUTPUT

Host:      $HOSTNAME ("$HOSTDISPLAYNAME")
Address:   $HOSTADDRESS
State:     $HOSTSTATE
Date/Time: $LONGDATETIME

URL: $ICINGAWEB2URL/monitoring/service/show?host=$HOSTNAME
Comment (optional): [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT

NMS-ID: $NMS_ID
TEMPLATE
    else
        if [ "T$HOSTSTATE" = 'TDOWN' ]
        then
            # Text message - max. 160 characters
            mail -s "NMS" \
              -aFrom:$MAILFROM -aReply-To:${DEFAULT_EMAIL_SENDER} \
              $PRIO_HEADERS $UE <<TEMPLATE
$NOTIFICATIONTYPE - $HOSTDISPLAYNAME is $HOSTSTATE
$HOSTADDRESS $LONGDATETIME
Comment: [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT
NMS-ID: $NMS_ID
TEMPLATE
        fi
    fi
done
