#!/bin/bash
logger -it icinga2-mail -- $0 $@

# ADAPT THESE to your environment:
DEFAULT_EMAIL_RECIPIENT='consult@btoy1.net'
DEFAULT_EMAIL_SENDER='consult@btoy1.net'
DEFAULT_WEBURL="http://localhost/icingaweb2"

## Function helpers
Usage() {
cat << EOF

Required parameters:
  -4 HOSTADDRESS (address)
  -d LONGDATETIME (icinga.long_date_time)
  -e SERVICENAME (service.name)
  -l HOSTNAME (host.name)
  -n HOSTDISPLAYNAME (host.display_name)
  -o SERVICEOUTPUT (service.output)
  -r USEREMAIL (user.email)
  -s SERVICESTATE (service.state)
  -t NOTIFICATIONTYPE (notification.type)
  -u SERVICEDISPLAYNAME (service.display_name)

Optional parameters:
  -6 HOSTADDRESS6 (address6)
  -b NOTIFICATIONAUTHORNAME (notification.author)
  -c NOTIFICATIONCOMMENT (notification.comment)
  -i ICINGAWEB2URL (notification_icingaweb2url, Default: unset)
  -f MAILFROM (notification_mailfrom, requires GNU mailutils (Debian/Ubuntu) or mailx (RHEL/SUSE))
  -v (notification_sendtosyslog, Default: false)

EOF
}

Help() {
    Usage;
    exit 0;
}

Error() {
    if [ "$1" ]; then
      echo $1
    fi
    Usage;
    exit 1;
}

## Main
while getopts 4:6:b:c:d:e:f:hi:l:n:o:r:s:t:u:v: opt
do
  case "$opt" in
    4) HOSTADDRESS=$OPTARG ;; # required
    6) HOSTADDRESS6=$OPTARG ;;
    b) NOTIFICATIONAUTHORNAME=$OPTARG ;;
    c) NOTIFICATIONCOMMENT=$OPTARG ;;
    d) LONGDATETIME=$OPTARG ;; # required
    e) SERVICENAME=$OPTARG ;; # required
    f) MAILFROM=$OPTARG ;;
    h) Usage ;;
    i) ICINGAWEB2URL=$OPTARG ;;
    l) HOSTNAME=$OPTARG ;; # required
    n) HOSTDISPLAYNAME=$OPTARG ;; # required
    o) SERVICEOUTPUT=$OPTARG ;; # required
    r) USEREMAIL=$OPTARG ;; # required
    s) SERVICESTATE=$OPTARG ;; # required
    t) NOTIFICATIONTYPE=$OPTARG ;; # required
    u) SERVICEDISPLAYNAME=$OPTARG ;; # required
    v) VERBOSE=$OPTARG ;;
   \?) echo "ERROR: Invalid option -$OPTARG" >&2
       Usage ;;
    :) echo "Missing option argument for -$OPTARG" >&2
       Usage ;;
    *) echo "Unimplemented option: -$OPTARG" >&2
       Usage ;;
  esac
done

shift $((OPTIND - 1))

# Some defaults for possibly missing parameters
[ ! "$USEREMAIL" ] && USEREMAIL="${DEFAULT_EMAIL_RECIPIENT}"

## Check required parameters
## Keep formatting in sync with mail-host-notification.sh
for P in HOSTADDRESS LONGDATETIME HOSTNAME HOSTDISPLAYNAME SERVICENAME SERVICEDISPLAYNAME SERVICEOUTPUT SERVICESTATE USEREMAIL NOTIFICATIONTYPE
do
    eval "PAR=\$${P}"
    if [ ! "$PAR" ]
    then
        Error "Parameter '$P' is missing"
    fi
done

[ -z "$ICINGAWEB2URL" ] && ICINGAWEB2URL="${DEFAULT_WEBURL}"
[ -z "$MAILFROM" ] && MAILFROM="${DEFAULT_EMAIL_SENDER}"

PRIO_HEADERS=''
case ${SERVICESTATE^^} in
CRITICAL)
    PRIO_HEADERS='-aX-Priority:1 -aPriority:High -aX-MSMail-Priority:High -aImportance:High'
    ;;
WARNING)
    # Send warnings only every 30 minutes
    LAST_WARNING_FILE="/tmp/${HOSTNAME}.${SERVICENAME}.last_warning"
    THIRTY_MIN_AGO=$(date +%s -d '30 min ago')
    if [ -s $LAST_WARNING_FILE ]
    then
        LAST_WARNING=$(date +%s --reference=$LAST_WARNING_FILE)
    else
        LAST_WARNING=0
    fi
    if [ $LAST_WARNING -gt $THIRTY_MIN_AGO ]
    then
        # Last warning was less than 30 minutes ago
        exit 0
    fi
    date > $LAST_WARNING_FILE

    PRIO_HEADERS='-aX-Priority:2'
    ;;
UNKNOWN)
    # We don't really care about these ...
    exit 0
    ;;
esac
NMS_ID=$(date +NMS-%FT%T-%N'-'$RANDOM)
for UE in $(awk '{gsub(/,/," ");print}' <<< $USEREMAIL)
do
    if [ -z $(grep -P '^\d{10}' <<< $UE) ]
    then
        # Standard email
        mail -s "$NOTIFICATIONTYPE - $HOSTDISPLAYNAME - $SERVICEDISPLAYNAME is $SERVICESTATE" \
          -aFrom:$MAILFROM -aReply-to:${DEFAULT_EMAIL_SENDER} \
          $PRIO_HEADERS $UE <<TEMPLATE
***** Digtrack Network Monitoring System  *****

Notification Type: $NOTIFICATIONTYPE
 $SERVICEOUTPUT

Service:   $SERVICENAME ("$SERVICEDISPLAYNAME")
Host:      $HOSTNAME ("$HOSTDISPLAYNAME")
Address:   $HOSTADDRESS
State:     $SERVICESTATE
Date/Time: $LONGDATETIME

URL: $ICINGAWEB2URL/monitoring/service/show?host=$HOSTNAME&service=$SERVICENAME
Comment (optional): [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT

NMS-ID: $NMS_ID
TEMPLATE
    else
        if [ "T$SERVICESTATE" = 'TCRITICAL' ]
        then
            # Text message - max. 160 characters
            mail -s "NMS" \
              -aFrom:$MAILFROM -aReply-To:${DEFAULT_EMAIL_SENDER} \
              $PRIO_HEADERS $UE <<TEMPLATE
$NOTIFICATIONTYPE - $HOSTDISPLAYNAME - $SERVICEDISPLAYNAME is $SERVICESTATE
$HOSTADDRESS $LONGDATETIME
Comment: [$NOTIFICATIONAUTHORNAME] $NOTIFICATIONCOMMENT

NMS-ID: $NMS_ID
TEMPLATE
        fi
    fi
done
