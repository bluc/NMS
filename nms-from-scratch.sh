#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

if [ "T$(uname -i)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 0
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Get the code from the repository
cd /usr/local/src
wget 'https://gitlab.com/bluc/NMS/-/archive/master/NMS-master.zip' -O NMS-master.zip
unzip -qo NMS-master.zip

#--------------------------------------------------------------------
# Ask for a new password for the 'nms' account
PROPOSED_PW="$(openssl rand -base64 48 | sed 's@[/i0oIl=+]@@g' | cut -c1-16)"
read -p "Please input new password for 'nms' account [ENTER='$PROPOSED_PW'] ? " PW
[ -z "$PW" ] && PW="$PROPOSED_PW"
find NMS-master -type f -print0 | xargs -0rn 40 sed -i "s/b3kkajudd/$PW/g"
read -p "Remember this password: '$PW' (press ENTER to continue)" YN

#--------------------------------------------------------------------
# Copy some files into the local file system
cd NMS-master/usr/local
cp sbin/nms.postinstall.sh sbin/SetupOneHost.sh sbin/nms-checks.sh /usr/local/sbin/
cp etc/* /usr/local/etc

#--------------------------------------------------------------------
# Install the software
/usr/local/sbin/nms.postinstall.sh
