#!/usr/bin/env bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Get the newest OUIs (if necessary)
GET_OUI=0
if [ -s /tmp/oui.txt ]
then
    # Get a newer copy if it is older than one week
    ONE_WEEK_AGO=$(date '+%s' -d '7 days ago')
    [ $(date '+%s' -r /tmp/oui.txt) -lt $ONE_WEEK_AGO ] && GET_OUI=1
else
    GET_OUI=1
fi
[ $GET_OUI -ne 0 ] && wget -qO /tmp/oui.txt http://linuxnet.ca/ieee/oui.txt
[ -s /tmp/oui.txt ] || exit 1
chmod 0666 /tmp/oui.txt

#--------------------------------------------------------------------
# Example: 192.168.240.1 192.168.240.34 192.168.240.37 192.168.240.8
for HOST_IP in $@
do
    # Make sure that we get an ARP entry
    ping -c 2 $HOST_IP &> /dev/null
    MAC_PREFIX=$(arp -an $HOST_IP | awk -F: '{gsub (/^.*at /,"");print $1"-"$2"-"$3}')
    [ -z "$MAC_PREFIX" ] && continue
    MAC_VENDOR=$(awk "/^${MAC_PREFIX^^}/"'{$1="";$2="";print}' /tmp/oui.txt )
    MAC_VENDOR=${MAC_VENDOR#"${MAC_VENDOR%%[![:space:]]*}"}
    [ -z "$MAC_VENDOR" ] && MAC_VENDOR='UNDEFINED'
    echo "$MAC_VENDOR"
done

#--------------------------------------------------------------------
# We are done
exit 0
