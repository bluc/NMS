#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

# Make sure we remove temporary files at exit
trap "rm -f /tmp/$$*" EXIT

#--------------------------------------------------------------------
# Global options
DEFAULT_VAL=9999999

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- = *x* ]] && DEBUG='-x'

#--------------------------------------------------------------------
# Global variables
declare -A CREDENTIALS
CREDENTIALS[nms_userid]='nms'
CREDENTIALS[nms_passwd]='nmsm0n'
CREDENTIALS[nms_snmp_community]='nms'
CREDENTIALS[fortigate_userid]='admin'
CREDENTIALS[fortigate_passwd]='Kev2Ruch5'
CREDENTIALS[bigip_userid]='admin'
CREDENTIALS[bigip_passwd]='bicknell'

#--------------------------------------------------------------------
if [ -s /usr/lib/nagios/plugins/utils.sh ]
then
    # Bring in NAGIOS utility functions
    source /usr/lib/nagios/plugins/utils.sh
else
    # Nagios return codes
    STATE_OK=0
    STATE_WARNING=1
    STATE_CRITICAL=2
    STATE_UNKNOWN=3

    #
    # check_range takes a value and a range string, returning successfully if an
    # alert should be raised based on the range.  Range values are inclusive.
    # Values may be integers or floats.
    #
    # Example usage:
    #
    # Generating an exit code of 1:
    # check_range 5 2:8
    #
    # Generating an exit code of 0:
    # check_range 1 2:8
    #
    check_range() {
	local v range yes no err decimal start end cmp match
	v="$1"
	range="$2"

	# whether to raise an alert or not
	yes=0
	no=1
	err=2

	# regex to match a decimal number
	decimal="-?([0-9]+\.?[0-9]*|[0-9]*\.[0-9]+)"

	# compare numbers (including decimals), returning true/false
	cmp() { awk "BEGIN{ if ($1) exit(0); exit(1)}"; }

	# returns successfully if the string in the first argument matches the
	# regex in the second
	match() { echo "$1" | grep -E -q -- "$2"; }

	# make sure value is valid
	if ! match "$v" "^$decimal$"; then
		echo "${0##*/}: check_range: invalid value" >&2
		unset -f cmp match
		return "$err"
	fi

	# make sure range is valid
	if ! match "$range" "^@?(~|$decimal)(:($decimal)?)?$"; then
		echo "${0##*/}: check_range: invalid range" >&2
		unset -f cmp match
		return "$err"
	fi

	# check for leading @ char, which negates the range
	if match $range '^@'; then
		range=${range#@}
		yes=1
		no=0
	fi

	# parse the range string
	if ! match "$range" ':'; then
		start=0
		end="$range"
	else
		start="${range%%:*}"
		end="${range#*:}"
	fi

	# do the comparison, taking positive ("") and negative infinity ("~")
	# into account
	if [ "$start" != "~" ] && [ "$end" != "" ]; then
		if cmp "$start <= $v" && cmp "$v <= $end"; then
			unset -f cmp match
			return "$no"
		else
			unset -f cmp match
			return "$yes"
		fi
	elif [ "$start" != "~" ] && [ "$end" = "" ]; then
		if cmp "$start <= $v"; then
			unset -f cmp match
			return "$no"
		else
			unset -f cmp match
			return "$yes"
		fi
	elif [ "$start" = "~" ] && [ "$end" != "" ]; then
		if cmp "$v <= $end"; then
			unset -f cmp match
			return "$no"
		else
			unset -f cmp match
			return "$yes"
		fi
	else
		unset -f cmp match
		return "$no"
	fi
    }
fi

#--------------------------------------------------------------------
# Custom SSH function
myssh() {
    local SSHCMD CMDArgs CONNECT_IP RC
    SSHCMD="$1"
    shift
    CMDArgs="$@"
    CONNECT_IP=$(grep -oP " (${CREDENTIALS[nms_userid]}\@\S+)" <<< $CMDArgs)
    $SSHCMD -o PasswordAuthentication=no $CMDArgs
    RC=$?
    if [ $RC -ne 0 ]
    then
        logger --priority daemon.err --id=$$ --tag NMS -- Key-based SSH connection failed to $CONNECT_IP with error code $RC
        if [ -x /usr/bin/sshpass ]
        then
            SSHPASS="${CREDENTIALS[nms_passwd]}" sshpass -e $SSHCMD $CMDArgs
            RC=$?
            if [ $RC -ne 0 ]
            then
                echo "ERROR: Can't establish ssh connection to '$CONNECT_IP' (error code $RC)"
                exit $STATE_UNKNOWN
            else
                logger --priority daemon.err --id=$$ --tag NMS -- Created password-based SSH connection to $CONNECT_IP
                return 0
            fi
        else
            echo "ERROR: Can't establish ssh connection to '$CONNECT_IP' (error code $RC)"
            exit $STATE_UNKNOWN
        fi
    else
        [[ $CMDArgs =~ ControlPersist ]] && logger --priority daemon.err --id=$$ --tag NMS -- Created key-based master SSH connection to $CONNECT_IP
        return 0
    fi
}

#--------------------------------------------------------------------
# Use syslog to decouple logging from processing
logger --priority daemon.notice --id=$$ --tag NMS -- $PROG $@

#--------------------------------------------------------------------
# Process options
VERBOSE=0
CONN_PROGRAM='/usr/bin/ssh'
CONN_TIMEOUT=30
CONN_PORT=22
CONN_PROTOCOL=tcp
CONN_USER=${CREDENTIALS[nms_userid]}
CONN_KEY='/var/lib/nagios/.ssh/id_nms'
HOST_IP=''
HOST_OS=''
SNMP_VERS='2c'
SNMP_COMMUNITY="${CREDENTIALS[nms_snmp_community]}"
SNMP_MIBS="-v $SNMP_VERS"' -M +/var/lib/mibs/ietf:/var/lib/mibs/iana:/usr/local/etc -m ALL -Pd -Pe -PR'
THRESHOLD_WARN=$DEFAULT_VAL
THRESHOLD_CRIT=$DEFAULT_VAL
MIN_VALUE=$DEFAULT_VAL
MAX_VALUE=$DEFAULT_VAL
# values needed when checking via a proxy
PROXY_IP=''
NAT_IP=''
NAT_TYPE=''
# Extra parameters
X_PARAMS=''

while getopts "Vvhe:w:c:t:H:O:p:P:R:u:k:m:M:X:" OPTION
do
    case "${OPTION}" in
    V)  echo "$PROG $VERSION"
        printf '%b' "The local plugins come with ABSOLUTELY NO WARRANTY. You may redistribute\ncopies of the plugins under the terms of the GNU General Public License.\nFor more information about these matters, see the file named COPYING.\n"
        exit 0
        ;;
    v)  VERBOSE=1
        ;;
    e)  CONN_PROGRAM=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    p)  CONN_PORT=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    P)  CONN_PROTOCOL=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    R)  PROXY_IP=$(cut -d: -f1 <<< $OPTARG)
        NAT_IP=$(cut -d: -f2 <<< $OPTARG)
        NAT_TYPE=$(cut -d: -f3 <<< $OPTARG)
        ;;
    t)  CONN_TIMEOUT=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    u)  CONN_USER=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        if [[ $CONN_USER =~ % ]]
        then
            # Seperate the userid and password
            CREDENTIALS[nms_passwd]=${CONN_USER##*%}
            CONN_USER=${CONN_USER%%%*}
        fi
        ;;
    k)  CONN_KEY=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    w)  THRESHOLD_WARN=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    c)  THRESHOLD_CRIT=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    H)  HOST_IP=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    O)  HOST_OS=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    m)  MIN_VALUE=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    M)  MAX_VALUE=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    X)  X_PARAMS=$(sed "s/ //g;s/'//g" <<< $OPTARG)
        ;;
    h)  cat << EOT
Usage: ${PROG} [options] [-- arguments]
       -e program   Use 'program' to connect to remote host [default=$CONN_PROGRAM]
       -t seconds   Wait at most 'seconds' to connect to remote host [default=$CONN_TIMEOUT]
       -w number    Threshold for warning messages [default=$THRESHOLD_WARN]
       -c number    Threshold for critical messages [default=$THRESHOLD_CRIT]
       -m number    Minimum allowed value [default=$MIN_VALUE]
       -M number    Maximum allowed value [default=$MAX_VALUE]
       -H address   IP address of host to connect to [default=none, MUST be specified]
       -O string    Operating system host to connect to [default=none, MUST be specified]
       -p number    Port on host to connect to [default=$CONN_PORT]
       -P [tcp|udp] Protocol to connect to host [default=$CONN_PROTOCOL]
       -u userid    User to connect as [default=$CONN_USER]
                    Special case: "id%password" sets the password for the user "id"
       -k path      File path for SSH key [default=$CONN_KEY]
       -X "string"  Extra parameters
       -v           Show verbose output [default=no]
       -V           Show version (exits script)
       -h           Show this help (exits script)
       -P proxy_ip:nat_ip:nat_type
                    Set proxy parameters [default=none]
                    Example: -R 192.168.0.190:192.168.0.61:bigip

EOT
        printf '%b' "Send email to consult@btoy1.net if you have questions regarding use\nof this software. To submit patches or suggest improvements, send email to\nconsult@btoy1.net. Please include version information with all\ncorrespondence (when possible, use output from the -V option of the\nplugin itself).\n"
        exit $STATE_UNKNOWN
        ;;
    esac
done
shift $((OPTIND-1))

#############################
# Some sanity checks
if [ -z "$HOST_IP" ]
then
    echo 'ERROR: Missing host address'
    exit $STATE_UNKNOWN
fi

if [ ! -x $CONN_PROGRAM ]
then
    echo "ERROR: Program '$CONN_PROGRAM' not found"
    exit $STATE_UNKNOWN
fi

if [ -z "$HOST_OS" ]
then
    echo 'ERROR: Missing host operating system'
    exit $STATE_UNKNOWN
fi
if [ $MIN_VALUE -gt $MAX_VALUE ]
then
    echo "Minimum value can not exceed maximum value"
    exit $STATE_UNKNOWN
fi

#############################
# Create the cache directory
CACHE_DIR=/var/cache/icinga2/bluc-cache
mkdir -p $CACHE_DIR

#############################
# Set OS specific options
case "${HOST_OS}" in
SunOS|Linux)
    if [[ ! $CONN_PROGRAM =~ ssh ]]
    then
        echo "ERROR: Only 'ssh' is supported under $HOST_OS"
        exit $STATE_UNKNOWN
    fi
    ;;
Windows)
    if [[ ! $CONN_PROGRAM =~ wmic ]]
    then
        echo "ERROR: Only 'wmic' is supported under $HOST_OS"
        exit $STATE_UNKNOWN
    fi
    ;;
Fortigate)
    case $CONN_PROGRAM in
    *snmpget)
        CONN_PROTOCOL=udp
# Only needed for SNMP v3
#        SNMP_PASSWORD='n3aphEnwubU'
         ;;
    *ssh)
         :
         ;;
    *)
        echo "ERROR: Only 'ssh' and 'snmpget' are supported under $HOST_OS"
        exit $STATE_UNKNOWN
    esac
    ;;
Cisco|bigip)
    if [[ $CONN_PROGRAM =~ snmpget ]]
    then
        CONN_PROTOCOL=udp
# Only needed for SNMP v3
#        SNMP_PASSWORD='n3aphEnwubU'
    else
        echo "ERROR: Only 'snmpget' is supported under $HOST_OS"
        exit $STATE_UNKNOWN
    fi
    ;;
Windows)
    if [[ $CONN_PROGRAM =~ snmpget ]]
    then
        CONN_PROTOCOL=tcp
# Only needed for SNMP v3
#        SNMP_PASSWORD='n3aphEnwubU'
    fi
    ;;
*)
    echo "Unsupported operating system '$HOST_OS'"
    exit $STATE_UNKNOWN
    ;;   
esac

#############################
# Handle possible proxy checking
if [ ! -z "$PROXY_IP" ]
then
    CACHE_NAME="PROXY-public_${HOST_IP}-nat_${NAT_IP}-${NAT_TYPE}-proxy_${PROXY_IP}"
    REBUILD_CACHE_FILE=0
    if [ -s $CACHE_DIR/$CACHE_NAME ]
    then
        # We need to rebuild the cache file if it is older than day
        #  AND the current hour is 3
        if [ $(date +%k) -eq 3 ]
        then
            [ $(date +%s --reference=$CACHE_DIR/$CACHE_NAME) -lt $(date +%s -d '1 day ago') ] && REBUILD_CACHE_FILE=1
        fi
    else
        # We need to rebuild the cache file
        REBUILD_CACHE_FILE=1
    fi
    if [ $REBUILD_CACHE_FILE -ne 0 ]
    then
        case "$NAT_TYPE" in
        bigip)
            snmpget $SNMP_MIBS -c $SNMP_COMMUNITY udp:${NAT_IP}:161 sysDescr.0 > /tmp/$$.out.nat 2> /tmp/$$.err
            if [ -z "$(grep 'Linux' /tmp/$$.out.nat)" ]
            then
                # This is an older F5
                snmpget $SNMP_MIBS -c $SNMP_COMMUNITY udp:${NAT_IP}:161 memberAddress.${HOST_IP}.0.0 > /tmp/$$.out.nat 2> /tmp/$$.err
                PRIV_IP=$(awk '{print $NF}' /tmp/$$.out.nat)
            else
                # Try newer MIBs
                snmpget $SNMP_MIBS -c $SNMP_COMMUNITY udp:${NAT_IP}:161 ltmVirtualServDefaultPool.\"/Common/${HOST_IP}\" > /tmp/$$.out.nat 2> /tmp/$$.err
                if [ -s /tmp/$$.out.nat -a -z "$(grep 'No Such Instance' /tmp/$$.out.nat)" ]
                then
                    POOL_NAME=$(awk '{print $NF}' /tmp/$$.out.nat)
                    snmpbulkwalk $SNMP_MIBS -c $SNMP_COMMUNITY udp:${NAT_IP}:161 ltmPoolMemberAddr.\"${POOL_NAME//\//\\\/}\" > /tmp/$$.out.nat 2> /tmp/$$.err
                    PRIV_IP=$(awk '/ltmPoolMemberAddr/{printf "%d.%d.%d.%d\n","0x"$(NF-3),"0x"$(NF-2),"0x"$(NF-1),"0x"$NF}' /tmp/$$.out.nat)
                else
                    snmpbulkwalk $SNMP_MIBS -c $SNMP_COMMUNITY udp:${NAT_IP}:161 f5 > /tmp/$$.out.nat 2>/dev/null
                    HEX_IP=$(printf '%02X ' ${HOST_IP//./ } | sed 's/ $//')
                    VS_NAME=$(awk -F\" '/ltmVirtualServAddr.*'"${HEX_IP}"'/{print $2}' /tmp/$$.out.nat)
                    POOL_NAME=$(awk '/ltmVirtualServDefaultPool.*'"$VS_NAME"'/{print $NF}' /tmp/$$.out.nat)
                    PRIV_IP=$(awk '/ltmPoolMemberStatAddr.*'"$POOL_NAME".*Hex-STRING'/{printf "%d.%d.%d.%d\n","0x"$(NF-3),"0x"$(NF-2),"0x"$(NF-1),"0x"$NF}' /tmp/$$.out.nat)
                fi
            fi
            if [ -z "$PRIV_IP" ]
            then
                echo "Couldn't find private IP for '$HOST_IP'"
                exit $STATE_UNKNOWN
            fi
            cat << EOT > $CACHE_DIR/$CACHE_NAME
# The name of this file
CACHE_NAME='$CACHE_NAME'
# The public address for this device
HOST_IP='$HOST_IP'
# The private address behind the NAT device
PRIV_IP='$PRIV_IP'
# The NAT device
NAT_IP='$NAT_IP'
NAT_TYPE='$NAT_TYPE'
# The corresponding proxy
PROXY_IP='$PROXY_IP'

EOT
            ;;
        *)
            echo "Unsupported NAT type '$NAT_TYPE'"
            exit $STATE_UNKNOWN
        esac
    else
        # Read in the cached info
        source $CACHE_DIR/$CACHE_NAME
    fi

    # Establish the master connection to the proxy
    SSH_SOCKET=/var/run/icinga2/${CREDENTIALS[nms_userid]}@${PROXY_IP}:22
    myssh /usr/bin/ssh -o ControlPersist=$((8 * 60 * 60)) -4 -o ControlMaster=yes \
      -o ControlPath=$SSH_SOCKET -4 -F /var/lib/nagios/.ssh/config \
      -i /var/lib/nagios/.ssh/id_nms -o ConnectTimeout=30 -p 22 ${CREDENTIALS[nms_userid]}@${PROXY_IP} \
      'exit 0' &> /dev/null
fi
# Set this in case we don't use a proxy for Windows hosts
[ -z "$PRIV_IP" ] && PRIV_IP=$HOST_IP

#############################
# Setup program specific options
case "${CONN_PROGRAM}" in
*ssh)
    if [ ! -s $CONN_KEY ]
    then
        echo "ERROR: SSH key '$CONN_KEY' not found"
        exit $STATE_UNKNOWN
    fi

    if [ -z "$(grep -w $CONN_USER ${CONN_KEY}.pub)" ]
    then
        echo "ERROR: User '$CONN_USER' not found in SSH key '$CONN_KEY'"
        exit $STATE_UNKNOWN
    fi

    # Setup a socket for a multiplexed SSH session
    cat << EOT > /var/lib/nagios/.ssh/config
Host *
    PreferredAuthentications publickey,password,keyboard-interactive
    # Support older SSH servers (see https://www.openssh.com/legacy.html):
    KexAlgorithms +diffie-hellman-group1-sha1
    HostKeyAlgorithms +ssh-dss
EOT
    SSH_SOCKET=/var/run/icinga2/${CONN_USER}@${HOST_IP}:$CONN_PORT
    if [ ! -S $SSH_SOCKET  ]
    then
        # Establish the master connection
        myssh $CONN_PROGRAM -o ControlPersist=$((8 * 60 * 60)) -4 -o ControlMaster=yes \
          -o ControlPath=$SSH_SOCKET -4 -F /var/lib/nagios/.ssh/config \
          -i $CONN_KEY -o ConnectTimeout=$CONN_TIMEOUT -p $CONN_PORT ${CONN_USER}@${HOST_IP} \
          'exit 0' &> /dev/null
    fi
    ;;
*snmpget)
    SNMP_COMMUNITY="${CREDENTIALS[nms_snmp_community]}"
    SNMP_MIBS="-v $SNMP_VERS"' -M +/var/lib/mibs/ietf:/var/lib/mibs/iana:/usr/local/etc -m ALL -Pd -Pe -PR'
    CONN_PORT=161
    ;;
esac
