#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

# Pull in the common framework for all check scripts
VERSION='1.0'  # This is needed for the '-V' option
source /usr/local/nagios-plugins/bluc_check_common.inc

#--------------------------------------------------------------------
# Get network interface data from the remote host
RETCODE=0
CMD=''
NOW=$(date +%s)
case $CONN_PROGRAM in
*ssh)
    # Get the interface stats
    #  Format: Iface RX-BYTES RX-ERR TX-BYTES TX-ERR
    case $HOST_OS in
    Linux)
        IF_CMD="awk '!/(Inter|face)/{sub(/:/,\"\",\$1);print \$1\" \"\$2\" \"\$4\" \"\$10\" \"\$12}' /proc/net/dev; for F in /sys/class/net/e*; do echo -n \"Speed for \$F: \"; cat \$F/speed; echo -n \"Status of \$F: \"; cat \$F/operstate; done"
        ;;
    SunOS)
        IF_CMD="netstat -in | awk '!/^(Name|Kernel|Iface)/{print \$1\" \"\$5*\$2\" \"\$6\" \"\$7*\$2\" \"\$8}'; kstat -p | egrep '(ifspeed|(link|mii)_status)' | egrep -v '(mac)'"
        ;;
    *)
        echo "ERROR: Unsupported operating system '$HOST_OS'"
        exit $STATE_UNKNOWN
        ;;
    esac

    CMD="$CONN_PROGRAM -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config \
         -o ConnectTimeout=$CONN_TIMEOUT -p $CONN_PORT ${CONN_USER}@${HOST_IP}"
    if [ -S $SSH_SOCKET  ]
    then
        # Use a multiplexed connection
        myssh $CMD -S $SSH_SOCKET "$IF_CMD" > /tmp/$$.out.ifstat 2> /tmp/$$.err
    else
        myssh $CMD "$IF_CMD" > /tmp/$$.out.ifstat 2> /tmp/$$.err
    fi
    if [ $? -ne 0 ]
    then
        echo "ERROR: Didn't get any output from '$HOST_IP' ("$(< /tmp/$$.err)")"
        exit $STATE_UNKNOWN
    fi
    ;;
*snmpget)
    case $HOST_OS in
    Fortigate)
        SSHPASS="${CREDENTIALS[fortigate_passwd]}" sshpass -e /usr/bin/ssh -4 -F /var/lib/nagios/.ssh/config -o ConnectTimeout=30 \
          -p 22 -o StrictHostKeyChecking=no ${CREDENTIALS[fortigate_userid]}@${HOST_IP} 'diagnose netlink interface list' | \
          awk '/^if/{printf "%s ",$0;getline;print}' >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
        SSHPASS="${CREDENTIALS[fortigate_passwd]}" sshpass -e /usr/bin/ssh -4 -F /var/lib/nagios/.ssh/config -o ConnectTimeout=30 \
          -p 22 -o StrictHostKeyChecking=no ${CREDENTIALS[fortigate_userid]}@${HOST_IP} 'diagnose netlink device list' | \
          awk '!/Interface/{printf "%s ",$0;getline;print}' >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
        ;;
    Cisco)
        snmpbulkwalk $SNMP_MIBS -c $SNMP_COMMUNITY ${CONN_PROTOCOL}:${HOST_IP}:${CONN_PORT} interfaces >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
        ;;
    bigip)
        snmpget $SNMP_MIBS -c $SNMP_COMMUNITY ${CONN_PROTOCOL}:${HOST_IP}:${CONN_PORT} sysDescr.0 > /tmp/$$.out.ifstat 2> /tmp/$$.err
        if [ -z "$(grep 'Linux' /tmp/$$.out.ifstat)" ]
        then
            # Older BigIP
            snmpbulkwalk $SNMP_MIBS -c $SNMP_COMMUNITY ${CONN_PROTOCOL}:${HOST_IP}:${CONN_PORT} vaddressEntry >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
        else
            # Newer BigIP
            snmpbulkwalk $SNMP_MIBS -c $SNMP_COMMUNITY ${CONN_PROTOCOL}:${HOST_IP}:${CONN_PORT} interfaces >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
        fi
        ;;
    *)
        echo "ERROR: Unsupported operating system '$HOST_OS'"
        exit $STATE_UNKNOWN
        ;;
    esac
    if [ $? -ne 0 ]
    then
        echo "ERROR: Didn't get any output from '$HOST_IP' ("$(< /tmp/$$.err)")"
        exit $STATE_UNKNOWN
    fi
    ;;
*wmic)
    declare -a WMIC_CMDS
    case $HOST_OS in
    Windows)
        WMIC_CMDS[0]="wmic --delimiter=, -U ${CONN_USER}%${CREDENTIALS[nms_passwd]} //$PRIV_IP 'select BytesReceivedPersec,BytesSentPersec,Timestamp_Perftime,Frequency_Perftime,PacketsOutboundErrors,PacketsReceivedErrors from Win32_PerfRawData_Tcpip_NetworkInterface'"
        ;;
    *)
        echo "ERROR: Unsupported operating system '$HOST_OS'"
        exit $STATE_UNKNOWN
        ;;
    esac
    if [ ! -z "$PROXY_IP" ]
    then
        i=0
        while [ $i -lt ${#WMIC_CMDS[@]} ]
        do
            myssh /usr/bin/ssh -o ControlPath=$SSH_SOCKET -4 -F /var/lib/nagios/.ssh/config \
              -i /var/lib/nagios/.ssh/id_nms -o ConnectTimeout=30 -p 22 ${CREDENTIALS[nms_userid]}@${PROXY_IP} \
              "${WMIC_CMDS[i]}" >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
            i=$(($i + 1))
       done
    else
        i=0
        while [ $i -lt ${#WMIC_CMDS[@]} ]
        do
            echo "${WMIC_CMDS[i]}" >> /tmp/$$.local-wmic
            i=$(($i + 1))
        done
        sh /tmp/$$.local-wmic >> /tmp/$$.out.ifstat 2>> /tmp/$$.err
    fi
    ;;
*)
    echo "ERROR: Only 'ssh', 'snmpget' and 'wmic' are supported at this time"
    exit $STATE_UNKNOWN
    ;;
esac

# Sanity checks
if [ ! -s /tmp/$$.out.ifstat ]
then
    echo "ERROR: Didn't get interface statistics from '$HOST_IP'"
    exit $STATE_UNKNOWN
fi

#--------------------------------------------------------------------
# Check for thresholds (in this case interface errors)
PERFDATA=''
CRIT=''
WARN=''

# Extract the correct infos
INT_COUNT=0
case $CONN_PROGRAM in
*snmpget)
    case $HOST_OS in
    Fortigate)
        INT_COUNT=$(grep -c 'if=' /tmp/$$.out.ifstat)
#        VPNS=$(awk '/fgVpnTunnelUpCount.0/{print $NF}' /tmp/$$.out.hw)
#        echo $(($INT_COUNT + $VPNS))" Network Interface(s):"
        IF_NUM=1
        while [ $IF_NUM -le $INT_COUNT ]
        do
            IF_NAME=$(awk '/index='$IF_NUM' /{gsub(/=/," ");print $2}' /tmp/$$.out.ifstat)
            # Get the stats for this interface
            awk "/${IF_NAME}"'.Rx/{printf "RX_BYTES=%d\nRX_ERROR=%d\nTX_BYTES=%d\nTX_ERROR=%d\n",$2,$4,$12,$14}' /tmp/$$.out.ifstat > /tmp/$$.out.include
            [ -s /tmp/$$.out.include ] || continue
            source /tmp/$$.out.include

            # Calculate the throughput since the last check
            if [ -s ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers ]
            then
                source ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers
                SECONDS_SINCE_LAST_CHECK=$(($NOW - $LAST_NOW))
                TX_BPS=$(($(($TX_BYTES - $LAST_TX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
                RX_BPS=$(($(($RX_BYTES - $LAST_RX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))

            else
                RX_BPS=0
                TX_BPS=0
                LAST_TX_BYTES=$TX_BYTES
                LAST_TX_ERROR=$TX_ERROR
                LAST_RX_BYTES=$RX_BYTES
                LAST_RX_ERROR=$RX_ERROR
            fi

            # Save the current values for the next check
            cat << EOT > ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers
# Last updated: $(date '+%F %T')
LAST_NOW=$(date '+%s')
LAST_TX_BYTES=$TX_BYTES
LAST_TX_ERROR=$TX_ERROR
LAST_RX_BYTES=$RX_BYTES
LAST_RX_ERROR=$RX_ERROR
EOT

            echo -n " txbps_${IF_NAME}=${TX_BPS/#-/} rxbps_${IF_NAME}=${RX_BPS/#-/} txbytes_${IF_NAME}=$TX_BYTES txerror_${IF_NAME}=$TX_ERROR;0 rxbytes_${IF_NAME}=$RX_BYTES rxerror_${IF_NAME}=$RX_ERROR;0" >> /tmp/$$.perfdata

            # Check for errors
            if [ $TX_ERROR -gt $(($TX_BYTES / 10)) ]
            then
                [ $(($TX_BYTES / 10)) -gt 0 ] && CRIT="$CRITICAL more than 10% errors in the outbound direction on '$IF_NAME'"
            elif [ $RX_ERROR -gt $(($RX_BYTES / 10)) ]
            then
                [ $(($RX_BYTES / 10)) -gt 0 ] && CRIT="$CRITICAL more than 10% errors in the inbound direction on '$IF_NAME'" 
            elif [ $TX_ERROR -gt $(($TX_BYTES / 100)) ]
            then
                [ $(($TX_BYTES / 100)) -gt 0 ] && WARN="$WARN more than 1% errors in the outbound direction on '$IF_NAME'"
            elif [ $RX_ERROR -gt $(($RX_BYTES / 100)) ]
            then
                [ $(($RX_BYTES / 100)) -gt 0 ] && WARN="$WARN more than 1% errors in the inbound direction on '$IF_NAME'" 
            elif [ $TX_ERROR -gt $LAST_TX_ERROR ]
            then
                WARN="$WARN increasing errors in the outbound direction on '$IF_NAME'"
            elif [ $RX_ERROR -gt $LAST_RX_ERROR ]
            then
                WARN="$WARN increasing errors in the inbound direction on '$IF_NAME'"
            fi
            IF_NUM=$(($IF_NUM + 1))
        done
        ;;
    Cisco)
        INT_COUNT=$(awk '/ifNumber.0/{print $NF}' /tmp/$$.out.ifstat)
        awk '/ifIndex/{print $NF}' /tmp/$$.out.ifstat | while read i
        do
            IF_NAME=$(awk -F\' '/ifDescr.'$i' /{gsub(/"/,"");print $(NF-1)}' /tmp/$$.out.ifstat)
            awk '/ifInOctets.'$i' /{printf "RX_BYTES=%d\n",$NF};
                 /ifOutOctets.'$i' /{printf "TX_BYTES=%d\n",$NF};
                 /ifInErrors.'$i' /{printf "RX_ERROR=%d\n",$NF}
                 /ifOutErrors.'$i' /{printf "TX_ERROR=%d\n",$NF}
                ' /tmp/$$.out.ifstat > /tmp/$$.out.include

            source /tmp/$$.out.include

            # Calculate the throughput since the last check
            if [ -s ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers ]
            then
                source ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers
                SECONDS_SINCE_LAST_CHECK=$(($NOW - $LAST_NOW))
                TX_BPS=$(($(($TX_BYTES - $LAST_TX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
                RX_BPS=$(($(($RX_BYTES - $LAST_RX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))

            else
                RX_BPS=0
                TX_BPS=0
                LAST_TX_BYTES=$TX_BYTES
                LAST_TX_ERROR=$TX_ERROR
                LAST_RX_BYTES=$RX_BYTES
                LAST_RX_ERROR=$RX_ERROR
            fi

            # Save the current values for the next check
            cat << EOT > ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers
# Last updated: $(date '+%F %T')
LAST_NOW=$(date '+%s')
LAST_TX_BYTES=$TX_BYTES
LAST_TX_ERROR=$TX_ERROR
LAST_RX_BYTES=$RX_BYTES
LAST_RX_ERROR=$RX_ERROR
EOT

            echo -n " txbps_${IF_NAME}=${TX_BPS/#-/} rxbps_${IF_NAME}=${RX_BPS/#-/} txbytes_${IF_NAME}=$TX_BYTES txerror_${IF_NAME}=$TX_ERROR;0 rxbytes_${IF_NAME}=$RX_BYTES rxerror_${IF_NAME}=$RX_ERROR;0" >> /tmp/$$.perfdata

            # Check for errors
            if [ $TX_ERROR -gt $(($TX_BYTES / 10)) ]
            then
                CRIT="$CRITICAL more than 10% errors in the outbound direction on '$IF_NAME'"
            elif [ $RX_ERROR -gt $(($RX_BYTES / 10)) ]
            then
                CRIT="$CRITICAL more than 10% errors in the inbound direction on '$IF_NAME'" 
            elif [ $TX_ERROR -gt $(($TX_BYTES / 100)) ]
            then
                WARN="$WARN more than 1% errors in the outbound direction on '$IF_NAME'"
            elif [ $RX_ERROR -gt $(($RX_BYTES / 100)) ]
            then
                WARN="$WARN more than 1% errors in the inbound direction on '$IF_NAME'" 
            elif [ $TX_ERROR -gt $LAST_TX_ERROR ]
            then
                WARN="$WARN increasing errors in the outbound direction on '$IF_NAME'"
            elif [ $RX_ERROR -gt $LAST_RX_ERROR ]
            then
                WARN="$WARN increasing errors in the inbound direction on '$IF_NAME'"
            fi
            IF_NUM=$(($IF_NUM + 1))
        done

        # Remove some extra infos from the stats file
        sed -i 's/^IF-MIB:://;s/STRING: //;s/INTEGER: //;s/Gauge32: //' /tmp/$$.out.ifstat
        ;;
    bigip)
        if [ -z "$(grep 'Linux' /tmp/$$.out.ifstat)" ]
        then
            # Older BigIP
            INT_COUNT=$(grep -c 'vaddressIndex\.' /tmp/$$.out.ifstat)
            awk '/vaddressIndex/{print $NF}' /tmp/$$.out.ifstat | while read i
            do
                IF_NAME=$(awk '/vaddressIpAddr.'$i' /{gsub(/"/,"");print $NF}' /tmp/$$.out.ifstat)

                awk '/vaddressBitsin.'$i' /{printf "RX_BYTES=%d\n",$NF};
                     /vaddressBitsout.'$i' /{printf "TX_BYTES=%d\n",$NF};
                    ' /tmp/$$.out.ifstat > /tmp/$$.out.include

                source /tmp/$$.out.include

                # Calculate the throughput since the last check
                if [ -s ${CACHE_DIR}/${HOST_IP}.${i}.lastnumbers ]
                then
                    source ${CACHE_DIR}/${HOST_IP}.${i}.lastnumbers
                    SECONDS_SINCE_LAST_CHECK=$(($NOW - $LAST_NOW))
                    TX_BPS=$(($(($TX_BYTES - $LAST_TX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
                    RX_BPS=$(($(($RX_BYTES - $LAST_RX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
                else
                    RX_BPS=0
                    TX_BPS=0
                    LAST_TX_BYTES=$TX_BYTES
                    LAST_RX_BYTES=$RX_BYTES
                fi

                # Save the current values for the next check
                cat << EOT > ${CACHE_DIR}/${HOST_IP}.${i}.lastnumbers
# Last updated: $(date '+%F %T')
LAST_NOW=$(date '+%s')
LAST_TX_BYTES=$TX_BYTES
LAST_RX_BYTES=$RX_BYTES
EOT

                echo -n " txbps_${IF_NAME}=${TX_BPS/#-/} rxbps_${IF_NAME}=${RX_BPS/#-/} txbytes_${IF_NAME}=$TX_BYTES rxbytes_${IF_NAME}=$RX_BYTES" >> /tmp/$$.perfdata

                IF_NUM=$(($IF_NUM + 1))
            done

            # Remove some extra infos from the stats file
            sed -i 's/^LOAD-BAL-SYSTEM-MIB:://;s/STRING: //;s/INTEGER: //;s/Gauge32: //;s/IpAddress: //' /tmp/$$.out.ifstat
        else
            # Newer BigIP
            INT_COUNT=$(awk '/ifNumber.0/{print $NF}' /tmp/$$.out.ifstat)
            awk '/ifIndex/{print $NF}' /tmp/$$.out.ifstat | while read i
            do
                IF_NAME=$(awk '/ifDescr.'$i' /{gsub(/"/,"");print $NF}' /tmp/$$.out.ifstat)
                awk '/ifInOctets.'$i' /{printf "RX_BYTES=%d\n",$NF};
                     /ifOutOctets.'$i' /{printf "TX_BYTES=%d\n",$NF};
                     /ifInErrors.'$i' /{printf "RX_ERROR=%d\n",$NF}
                     /ifOutErrors.'$i' /{printf "TX_ERROR=%d\n",$NF}
                    ' /tmp/$$.out.ifstat > /tmp/$$.out.include

                source /tmp/$$.out.include

                # Calculate the throughput since the last check
                if [ -s ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers ]
                then
                    source ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers
                    SECONDS_SINCE_LAST_CHECK=$(($NOW - $LAST_NOW))
                    TX_BPS=$(($(($TX_BYTES - $LAST_TX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
                    RX_BPS=$(($(($RX_BYTES - $LAST_RX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
                else
                    RX_BPS=0
                    TX_BPS=0
                    LAST_TX_BYTES=$TX_BYTES
                    LAST_TX_ERROR=$TX_ERROR
                    LAST_RX_BYTES=$RX_BYTES
                    LAST_RX_ERROR=$RX_ERROR
                fi

                # Save the current values for the next check
                cat << EOT > ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers
# Last updated: $(date '+%F %T')
LAST_NOW=$(date '+%s')
LAST_TX_BYTES=$TX_BYTES
LAST_TX_ERROR=$TX_ERROR
LAST_RX_BYTES=$RX_BYTES
LAST_RX_ERROR=$RX_ERROR
EOT

                echo -n " txbps_${IF_NAME}=${TX_BPS/#-/} rxbps_${IF_NAME}=${RX_BPS/#-/} txbytes_${IF_NAME}=$TX_BYTES txerror_${IF_NAME}=$TX_ERROR;0 rxbytes_${IF_NAME}=$RX_BYTES rxerror_${IF_NAME}=$RX_ERROR;0" >> /tmp/$$.perfdata

                # Check for errors
                if [ $TX_ERROR -gt $(($TX_BYTES / 10)) ]
                then
                    CRIT="$CRITICAL more than 10% errors in the outbound direction on '$IF_NAME'"
                elif [ $RX_ERROR -gt $(($RX_BYTES / 10)) ]
                then
                    CRIT="$CRITICAL more than 10% errors in the inbound direction on '$IF_NAME'" 
                elif [ $TX_ERROR -gt $(($TX_BYTES / 100)) ]
                then
                    WARN="$WARN more than 1% errors in the outbound direction on '$IF_NAME'"
                elif [ $RX_ERROR -gt $(($RX_BYTES / 100)) ]
                then
                    WARN="$WARN more than 1% errors in the inbound direction on '$IF_NAME'" 
                elif [ $TX_ERROR -gt $LAST_TX_ERROR ]
                then
                    WARN="$WARN increasing errors in the outbound direction on '$IF_NAME'"
                elif [ $RX_ERROR -gt $LAST_RX_ERROR ]
                then
                    WARN="$WARN increasing errors in the inbound direction on '$IF_NAME'"
                fi
                IF_NUM=$(($IF_NUM + 1))
            done

            # Remove some extra infos from the stats file
            sed -i 's/^IF-MIB:://;s/STRING: //;s/INTEGER: //;s/Gauge32: //' /tmp/$$.out.ifstat
        fi
        ;;
    esac
    ;;
*wmic)
    INT_COUNT=$(grep -c '^[0-3]' /tmp/$$.out.ifstat)
    # rxbps = BytesReceivedPersec * 8 / (Timestamp_PerfTime / Frequency_PerfTime)
    # txbps = BytesSentPersec * 8 / (Timestamp_PerfTime / Frequency_PerfTime)
    #  see also  https://social.msdn.microsoft.com/Forums/windowsdesktop/en-US/55a25dc0-1720-45b9-a123-27fa1693ebe4/wmiunderstanding-timestampperftime?forum=windowsgeneraldevelopmentissues
    awk -F, '/^[0-9]/{gsub(/ /,"_",$4);uptime=$7/$3;printf "%s %.0f %.0f %.0f %.0f\n",$4,$1*8/uptime,$2*8/uptime,$5,$6}' /tmp/$$.out.ifstat | while read IF_NAME RX_BPS TX_BPS TX_ERROR RX_ERROR
    do
        [[ $IF_NAME =~ Loopback ]] && continue
        echo -n " txbps_${IF_NAME}=${TX_BPS} rxbps_${IF_NAME}=${RX_BPS} txerror_${IF_NAME}=$TX_ERROR;0 rxerror_${IF_NAME}=$RX_ERROR;0" >> /tmp/$$.perfdata

        if [ -s ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers ]
        then
           source ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers
           SECONDS_SINCE_LAST_CHECK=$(($NOW - $LAST_NOW))
        else
            LAST_TX_ERROR=$TX_ERROR
            LAST_RX_ERROR=$RX_ERROR
        fi

        # Save the current values for the next check
        cat << EOT > ${CACHE_DIR}/${HOST_IP}.${IF_NAME//\//.}.lastnumbers
# Last updated: $(date '+%F %T')
LAST_NOW=$(date '+%s')
LAST_TX_ERROR=$TX_ERROR
LAST_RX_ERROR=$RX_ERROR
EOT

        # Check for errors
        if [ $TX_ERROR -gt $LAST_TX_ERROR ]
        then
            WARN="$WARN increasing errors in the outbound direction on '$IF_NAME'"
        elif [ $RX_ERROR -gt $LAST_RX_ERROR ]
        then
            WARN="$WARN increasing errors in the inbound direction on '$IF_NAME'"
        fi
    done
    ;;
*ssh)
    IF_NUM=0
    while read LINE
    do
        [ -z "$LINE" ] && continue
        [[ $LINE =~ : ]] && continue
        set $(cut -d" " -f1- <<<$LINE)
        [ $# -lt 5 ] && continue
        IF_NAME="$1"
        RX_BYTES="$2"
        RX_ERROR="$3"
        TX_BYTES="$4"
        TX_ERROR="$5"

        # Skip some interfaces that we don't care about
        case "$IF_NAME" in
        usbecm*|lo*|vmbr*)
            continue
            ;;
        esac

        # Calculate the throughput since the last check
        if [ -s ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers ]
        then
            source ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers
            SECONDS_SINCE_LAST_CHECK=$(($NOW - $LAST_NOW))
            TX_BPS=$(($(($TX_BYTES - $LAST_TX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
            RX_BPS=$(($(($RX_BYTES - $LAST_RX_BYTES)) / $SECONDS_SINCE_LAST_CHECK))
        else
            RX_BPS=0
            TX_BPS=0
        fi

        # Save the current values for the next check
        cat << EOT > ${CACHE_DIR}/${HOST_IP}.${IF_NAME}.lastnumbers
# Last updated: $(date '+%F %T')
LAST_NOW=$(date '+%s')
LAST_TX_BYTES=$TX_BYTES
LAST_TX_ERROR=$TX_ERROR
LAST_RX_BYTES=$RX_BYTES
LAST_RX_ERROR=$RX_ERROR
EOT

        echo -n " txbps_${IF_NAME}=${TX_BPS/#-/} rxbps_${IF_NAME}=${RX_BPS/#-/} txbytes_${IF_NAME}=$TX_BYTES txerror_${IF_NAME}=$TX_ERROR;0 rxbytes_${IF_NAME}=$RX_BYTES rxerror_${IF_NAME}=$RX_ERROR;0" >> /tmp/$$.perfdata

        # Check for errors
        if [ $TX_ERROR -gt $(($TX_BYTES / 10)) ]
        then
            CRIT="$CRITICAL more than 10% errors in the outbound direction on '$IF_NAME'"
        elif [ $RX_ERROR -gt $(($RX_BYTES / 10)) ]
        then
            CRIT="$CRITICAL more than 10% errors in the inbound direction on '$IF_NAME'" 
        elif [ $TX_ERROR -gt $(($TX_BYTES / 100)) ]
        then
            WARN="$WARN more than 1% errors in the outbound direction on '$IF_NAME'"
        elif [ $RX_ERROR -gt $(($RX_BYTES / 100)) ]
        then
            WARN="$WARN more than 1% errors in the inbound direction on '$IF_NAME'" 
        elif [ $TX_ERROR -gt $LAST_TX_ERROR ]
        then
            WARN="$WARN increasing errors in the outbound direction on '$IF_NAME'"
        elif [ $RX_ERROR -gt $LAST_RX_ERROR ]
        then
            WARN="$WARN increasing errors in the inbound direction on '$IF_NAME'"
        fi
        IF_NUM=$(($IF_NUM + 1))
    done < /tmp/$$.out.ifstat
    INT_COUNT=$IF_NUM
    ;;
esac

# Get the performance data (if present)
[ -s /tmp/$$.perfdata ] && PERFDATA=$(< /tmp/$$.perfdata)

if [ ! -z "$CRIT" ]
then
    echo "CRITICAL: $CRIT |$PERFDATA"
    for i in $(seq 0 $INT_COUNT)
    do
        awk '/escr\.'$i' /{print}
             /IpAddr\.'$i' /{print}
             /tatus\.'$i' /{print}
             /peed\.'$i' /{print}
            ' /tmp/$$.out.ifstat
        echo
    done
    exit $STATE_CRITICAL
fi
if [ ! -z "$WARN" ]
then
    echo "WARNING: $WARN |$PERFDATA"
    for i in $(seq 0 $INT_COUNT)
    do
        awk '/escr\.'$i' /{print}
             /IpAddr\.'$i' /{print}
             /tatus\.'$i' /{print}
             /peed\.'$i' /{print}
            ' /tmp/$$.out.ifstat
        echo
    done
    exit $STATE_WARNING
fi

echo "OK: No errors on any (physical or virtual) network interface |$PERFDATA"
for i in $(seq 0 $INT_COUNT)
do
    awk '/escr\.'$i' /{print}
         /IpAddr\.'$i' /{print}
         /tatus\.'$i' /{print}
         /peed\.'$i' /{print}
        ' /tmp/$$.out.ifstat
    echo
done
exit $STATE_OK
