#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

# Pull in the common framework for all check scripts
VERSION='1.0'  # This is needed for the '-V' option
source /usr/local/nagios-plugins/bluc_check_common.inc

#--------------------------------------------------------------------
# Get memory data from the remote host
RETCODE=0
CMD=''
case $CONN_PROGRAM in
*ssh)
    # Check for possible issues with postfix
    case $HOST_OS in
    Linux)
        LOGFILE='/var/log/mail.log'
        TAILPAR='-n 20000'
        ;;
    SunOS)
        LOGFILE='/var/log/syslog'
        TAILPAR='-20000'
        ;;
    *)
        echo "ERROR: Unsupported operating system '$HOST_OS'"
        exit $STATE_UNKNOWN
        ;;
    esac

    # Create the check script
    cat << EOT > /tmp/$$.check_postfix
#!/usr/bin/env bash
# Under Linux requires "nms" to be part of the "adm" group
tail $TAILPAR $LOGFILE > /tmp/\$\$.maillog
for MIN in 0 1 2 3 4 5
do
    X_MIN_AGO=\$(date '+%b %e %H:%M' -d "\$MIN min ago")
    egrep "\${X_MIN_AGO}.*(Relay access denied|Server configuration problem)" /tmp/\$\$.maillog >> /tmp/\$\$
done
[ -s /tmp/\$\$ ] && cat /tmp/\$\$
rm -f /tmp/\$\$ /tmp/\$\$.maillog
EOT
    CP_CMD='bash /tmp/check_postfix'

    CMD="$CONN_PROGRAM -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config \
         -o ConnectTimeout=$CONN_TIMEOUT -p $CONN_PORT ${CONN_USER}@${HOST_IP}"
    if [ -S $SSH_SOCKET  ]
    then
        # Use a multiplexed connection
        myssh scp -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config -o ControlPath=$SSH_SOCKET -o ConnectTimeout=$CONN_TIMEOUT \
            -P $CONN_PORT /tmp/$$.check_postfix ${CONN_USER}@${HOST_IP}:/tmp/check_postfix 2> /dev/null
        myssh $CMD -S $SSH_SOCKET "$CP_CMD" > /tmp/$$.out.postfix 2> /tmp/$$.err
    else
        # Use a standard connection
        myssh scp -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config -o ConnectTimeout=$CONN_TIMEOUT \
            -P $CONN_PORT /tmp/$$.check_postfix ${CONN_USER}@${HOST_IP}:/tmp/check_postfix 2> /dev/null
        myssh $CMD -S "$CP_CMD" > /tmp/$$.out.postfix 2> /tmp/$$.err
    fi
    if [ $? -ne 0 ]
    then
        echo "ERROR: Didn't get any output from '$HOST_IP' ("$(< /tmp/$$.err)")"
        exit $STATE_UNKNOWN
    fi
    ;;
*)
    echo "ERROR: Only 'ssh' is supported at this time"
    exit $STATE_UNKNOWN
    ;;
esac

#--------------------------------------------------------------------
# Check for thresholds (in this swap space and excessive RAM usage)
PERFDATA=''
CRIT=''
WARN=''

# Extract the correct infos
if [ -s /tmp/$$.out.postfix ]
then
    PROBLEMS=0
else
    PROBLEMS=$(sed -n '$=' /tmp/$$.out.postfix)
    [ -z "$PROBLEMS" ] && PROBLEMS=0
fi
[ $PROBLEMS -ne 0 ] && WARN="CRITICAL: Found '$PROBLEMS' postfix problems"
PERFDATA="postfix_problems=$PROBLEMS"

if [ ! -z "$WARN" ]
then
    echo "$WARN | $PERFDATA"
    cat /tmp/$$.out.postfix
    exit $STATE_WARNING
fi

echo "OK: No postfix problems found | $PERFDATA"
exit $STATE_OK
