#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

# Pull in the common framework for all check scripts
VERSION='1.0'  # This is needed for the '-V' option
source /usr/local/nagios-plugins/bluc_check_common.inc

#--------------------------------------------------------------------
# Get the all process data from the remote host
RETCODE=0
CMD=''
NOW=$(date "+%F %T")
if [[ $CONN_PROGRAM =~ ssh ]]
then

    # Copy the script to the remote host
    if [ "T$HOST_OS" = 'TLinux' -o "T$HOST_OS" = 'TSunOS' ]
    then
        # Create a script to copy to the remote host
        cat << EOS > /tmp/$$.check_sybase
#!/usr/bin/env bash
trap "rm -f /tmp/\$\$*" EXIT

if [ \$(date +%l) -eq 1 ]
then
    # Remove leftover output older than a day
    nice find /tmp -type f -user nms -mtime +1 -exec rm \-f '{}' ';'
fi

# Get the overall number of established connections to SyBase
echo -n 'Number of established Sybase connections: ';
netstat -an | grep -c '2638.*ESTABLISHED'

# See whether Sybase is running
SYBASE_RUN=\$(ps -eo comm | awk '/dbsr[v]/{print;exit}')
if [ -z "\$SYBASE_RUN" ]
then
    echo "Sybase is not running"
    exit 2
fi
SYBASE_VERS=\${SYBASE_RUN##*dbsrv}
DBNAME=\$(ps -eo comm,args | awk '/dbsr[v]/ && !/archive/{print \$4}')

case \$SYBASE_VERS in
7)
    if [ -s /opt/sybase/SYBSsa7/bin/asa_config.sh ]
    then
        source /opt/sybase/SYBSsa7/bin/asa_config.sh &> /dev/null
    else
        # the following lines set the SA 7 location.
        export PATH="/opt/sybase/SYBSsa7/bin:\$PATH"
        export LD_LIBRARY_PATH="/opt/sybase/SYBSsa7/lib:\$LD_LIBRARY_PATH"
        export ASANY="/opt/sybase/SYBSsa7"
    fi
    ;;

12)
    if [ -s /opt/sqlanywhere12/bin64/sa_config.sh ]
    then
        source /opt/sqlanywhere12/bin64/sa_config.sh &> /dev/null
    else
        # the following lines set the SA 12 location.
        export SQLANY12="/opt/sqlanywhere12"

        # the following lines add SA binaries to your path.
        export PATH="\$SQLANY12/bin64:\$SQLANY12/bin32:\${PATH:-}"

        # the following lines are required to load the various shared objects of SA
        export PATH="\$SQLANY12/sun/jre160/bin:\${PATH:-}"
        LD_LIBRARY_PATH="\$SQLANY12/sun/jre160/lib/sparcv9/client:\$SQLANY12/sun/jre160/lib/sparcv9/server:\$SQLANY12/sun/jre160/lib/sparcv9:\$SQLANY12/sun/jre160/lib/sparcv9/native_threads:\${LD_LIBRARY_PATH:-}"
        [ -z "\${JAVA_HOME:-}" ] && JAVA_HOME="\$SQLANY12/sun/jre160"
        export JAVA_HOME
        LD_LIBRARY_PATH="\$SQLANY12/lib64:\${LD_LIBRARY_PATH:-}"
        LD_LIBRARY_PATH="\$SQLANY12/lib32:\${LD_LIBRARY_PATH:-}"
        export LD_LIBRARY_PATH
    fi
    ;;

16)
    if [ -s /opt/sqlanywhere16/bin64/sa_config.sh ]
    then
        source /opt/sqlanywhere16/bin64/sa_config.sh &> /dev/null
    else
        # the following lines set the SA location.
        SQLANY16="/opt/sqlanywhere16"
        export SQLANY16

        [ -r "\$HOME/.sqlanywhere16/sample_env64.sh" ] && . "\$HOME/.sqlanywhere16/sample_env64.sh" 
        [ -z "\${SQLANYSAMP16:-}" ] && SQLANYSAMP16="/opt/sqlanywhere16/samples"
        export SQLANYSAMP16

        # the following lines add SA binaries to your path.
        PATH="\$SQLANY16/bin64:\${PATH:-}"
        export PATH

        # the following lines are required to load the various shared objects of SA
        PATH="\$SQLANY16/bin64/jre170/bin:\${PATH:-}"
        LD_LIBRARY_PATH="\$SQLANY16/bin64/jre170/lib/amd64/client:\$SQLANY16/bin64/jre170/lib/amd64/server:\$SQLANY16/bin64/jre170/lib/amd64:\$SQLANY16/bin64/jre170/lib/amd64/native_threads:\${LD_LIBRARY_PATH:-}"
        export PATH
        [ -z "\${JAVA_HOME:-}" ] && JAVA_HOME="\$SQLANY16/bin64/jre170"
        export JAVA_HOME
        LD_LIBRARY_PATH="\$SQLANY16/lib64:\${LD_LIBRARY_PATH:-}"
        export LD_LIBRARY_PATH
    fi
    ;;
*)
    echo "Unknown Sybase version '\$SYBASE_VERS'"
    exit 3
    ;;
esac
cat << EOT > /tmp/\$\$.sql
SELECT @@version, @@servername;
OUTPUT TO /tmp/\$\$.global.out FORMAT ASCII;
EOT

DBISQLC=\$(which dbisqlc)
if [ ! "T\${DBISQLC:0:1}" = 'T/' ]
then
    echo \$DBISQLC
    exit 4
fi

dbisqlc -c "dba,sql,\$DBNAME" -q read /tmp/\$\$.sql 2> /tmp/\$\$.err
if [ \$? -ne 0 ]
then
    cat << EOT
Didn't get global values from Sybase (\$DBNAME)

\$(< /tmp/\$\$.err)
EOT
    exit 2
fi

cat << EOT > /tmp/\$\$.sql
call sa_conn_info();
OUTPUT TO /tmp/\$\$.conn.out FORMAT ASCII;
EOT
dbisqlc -c "dba,sql,\$DBNAME" -q read /tmp/\$\$.sql 2> /tmp/\$\$.err
if [ \$? -ne 0 ]
then
    cat << EOT
Didn't get connection table from Sybase (\$DBNAME)

\$(< /tmp/\$\$.err)
EOT
    exit 2
fi
cat /tmp/\$\$.global.out
sed 's/^/CONN,/' /tmp/\$\$.conn.out

cat << EOT > /tmp/\$\$.sql
call sa_conn_properties( );
OUTPUT TO /tmp/\$\$.conn_props.out FORMAT ASCII;
EOT
dbisqlc -c "dba,sql,\$DBNAME" -q read /tmp/\$\$.sql 2> /tmp/\$\$.err
if [ \$? -ne 0 ]
then
    cat << EOT
Didn't get connection properties from Sybase (\$DBNAME)

\$(< /tmp/\$\$.err)
EOT
    exit 2
fi
awk '/AppInfo.*HOST=/ {print "PROPS,"\$0}' /tmp/\$\$.conn_props.out

exit 0
EOS
        if [ -S $SSH_SOCKET  ]
        then
            # Use a multiplexed connection
            myssh scp -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config -o ControlPath=$SSH_SOCKET -o ConnectTimeout=$CONN_TIMEOUT \
                -P $CONN_PORT /tmp/$$.check_sybase ${CONN_USER}@${HOST_IP}:/tmp/check_sybase 2> /dev/null
        else
            # Use a standard connection
            myssh scp -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config -o ConnectTimeout=$CONN_TIMEOUT -p $CONN_PORT \
              /tmp/$$.check_sybase ${CONN_USER}@${HOST_IP}:/tmp/check_sybase 2> /dev/null
        fi
        if [ $? -ne 0 ]
        then
            echo "ERROR: Couldn't copy check_sybase to '$HOST_IP' ("$(< /tmp/$$.err)")"
            exit $STATE_UNKNOWN
        fi

        CS_CMD='bash /tmp/check_sybase; rm -f /tmp/check_sybase'
    else
        echo "ERROR: Unsupported operating system '$HOST_OS'"
        exit $STATE_UNKNOWN
    fi

    CMD="$CONN_PROGRAM -i $CONN_KEY -4 -F /var/lib/nagios/.ssh/config \
         -o ConnectTimeout=$CONN_TIMEOUT -p $CONN_PORT ${CONN_USER}@${HOST_IP}"
    if [ -S $SSH_SOCKET  ]
    then
        # Use a multiplexed connection
        myssh $CMD -S $SSH_SOCKET "$CS_CMD" > /tmp/$$.out.sybase 2> /tmp/$$.err
    else
        # Use a standard connection
        myssh $CMD -S "$CS_CMD" > /tmp/$$.out.sybase 2> /tmp/$$.err
    fi
    if [ $? -ne 0 ]
    then
        echo "ERROR: Didn't get any output from '$HOST_IP' ("$(< /tmp/$$.err)")"
        exit $STATE_UNKNOWN
    fi
else
    echo "ERROR: Only 'ssh' is supported at this time"
    exit $STATE_UNKNOWN
fi

# Sanity checks
if [ ! -s /tmp/$$.out.sybase ]
then
    echo "ERROR: Didn't get any information for Sybase from '$HOST_IP'"
    exit $STATE_UNKNOWN
fi

#--------------------------------------------------------------------
# No fixed thresholds
PERFDATA=''
CRIT=''
WARN=''
RET_CODE=$STATE_OK

grep -m 1 "^'" /tmp/$$.out.sybase > /tmp/$$.out.sybase.version
if [ -s /tmp/$$.out.sybase.version ]
then
    # We have valid data
    # This MUST match the SELECT statement in the temp file
    set -- $(sed 's/,/ /g' /tmp/$$.out.sybase.version)
    SYBASE_VERSION="$1"
    SYBASE_DBNAME="$2"
    SYBASE_CONNS=$(awk '/^Number/{print $NF}' /tmp/$$.out.sybase)

    # Find any connection that is older than 12 hours
    case ${SYBASE_VERSION:1:1} in
    7|8|9)
        # Smaller number of connections
        SYBASE_MAX_CONN=180
        grep 'SQL_DBC_.*COMMIT' /tmp/$$.out.sybase | sed "s/'//g"  | \
          awk -F, '/^CONN/{sub(/ /,"T",$6);print $2" "$11" "$6}' > /tmp/$$.out.conntable
        AGO_STRING='5 min'
        ;;
    *)
        # Later version of Sybase
        SYBASE_MAX_CONN=2000
        grep 'SQL_DBC_.*COMMIT' /tmp/$$.out.sybase | sed "s/'//g" | \
          awk -F, '/^CONN/{sub(/ /,"T",$6);print $2" "$9" "$6}' > /tmp/$$.out.conntable
        AGO_STRING='12 hours'
        ;;
    esac
    AGO=$(date +%s -d "$AGO_STRING ago")
    sort -k 2 /tmp/$$.out.conntable | while read ID CLIENT DATE
    do
        [ -z "$DATE" ] && continue
        Conn_DATE=$(date +%s -d $DATE)
        if [ $Conn_DATE -lt $AGO ]
        then
            APP_INFO=$(awk -F, '/PROPS,'$ID'/{print $NF}' /tmp/$$.out.sybase)
            echo "=> Connection '$ID' from $CLIENT was started on ${DATE/T/ }" >> /tmp/$$.out.longconns
            echo "==> App Info: $APP_INFO" >> /tmp/$$.out.longconns
        fi
    done

    # Always extract the correct infos (if available)
    > /tmp/$$.conninfo
    if [ -s /tmp/$$.out.longconns ]
    then
        # The visual feedback
        LONGCONNS=$(($(sed -n '$=' /tmp/$$.out.longconns) / 2 ))
        echo "$LONGCONNS database connection(s) running for more than $AGO_STRING (as of $NOW):" >> /tmp/$$.conninfo
        echo 'Connections per pool:' >> /tmp/$$.conninfo
        awk '/Connection/ {count[$5]++} END {for (c_ip in count) print "=> "count[c_ip]" connections from "c_ip}' /tmp/$$.out.longconns | \
          sort -rnk 2 >> /tmp/$$.conninfo
        echo >> /tmp/$$.conninfo
        echo 'Connections per individual client host:' >> /tmp/$$.conninfo
        awk '/App Info/{gsub(/^.*HOST=|OS.*$/,"");count[$1]++} END {for (c_ip in count) print "=> "count[c_ip]" connections from "c_ip}' /tmp/$$.out.longconns | \
          sort -rnk 2 | sed 's/.$//' >> /tmp/$$.conninfo
        echo >> /tmp/$$.conninfo
        echo 'Details:' >> /tmp/$$.conninfo
        cat /tmp/$$.out.longconns >> /tmp/$$.conninfo

        # The graphic feedback
        awk '/Connection/ {count[$5]++} END {for (c_ip in count) printf " conns_pool_%s=%i",c_ip,count[c_ip]}' /tmp/$$.out.longconns > /tmp/$$.perfdata.pools
        echo >> /tmp/$$.perfdata.pools
        sed -i 's/[:;]//g' /tmp/$$.perfdata.pools
        awk '/App Info/{gsub(/^.*HOST=|OS.*$/,"");count[$1]++} END {for (c_ip in count) printf " conns_host_%s=%i",c_ip,count[c_ip]}' /tmp/$$.out.longconns > /tmp/$$.perfdata.hosts
        echo >> /tmp/$$.perfdata.hosts
        sed -i 's/[:;]//g' /tmp/$$.perfdata.hosts
    fi
    PERFDATA="conns=$SYBASE_CONNS;$SYBASE_WARN_CONN;$SYBASE_CRIT_CONN;0;$SYBASE_MAX_CONN"
    if [ -s /tmp/$$.perfdata.pools ]
    then
        PERFDATA="${PERFDATA}"$(< /tmp/$$.perfdata.pools)
        cp /tmp/$$.perfdata.pools $CACHE_DIR/${HOST_IP}.perfdata.pools
    fi
    if [ -s /tmp/$$.perfdata.hosts ]
    then
        PERFDATA="${PERFDATA}"$(< /tmp/$$.perfdata.hosts)
        cp /tmp/$$.perfdata.hosts $CACHE_DIR/${HOST_IP}.perfdata.hosts
    fi

    # Determine warning and critical levels dynamically
    SYBASE_WARN_CONN=$(awk '{per=$1/100;printf "%d",82*per}' <<< $SYBASE_MAX_CONN)
    SYBASE_CRIT_CONN=$(awk '{per=$1/100;printf "%d",89*per}' <<< $SYBASE_MAX_CONN)
    # Report possible warning and critical conditions
    if [ $SYBASE_CONNS -gt $SYBASE_CRIT_CONN ]
    then
        echo -n "CRITICAL: $SYBASE_CONNS is more than 89% of max. database connections ($SYBASE_MAX_CONN) | $PERFDATA"
        RET_CODE=$STATE_CRITICAL
    elif [ $SYBASE_CONNS -gt $SYBASE_WARN_CONN ]
    then
        echo -n "Warning: $SYBASE_CONNS is more than 82% of max. database connections ($SYBASE_MAX_CONN) | $PERFDATA"
        RET_CODE=$STATE_WARNING
    else
        RET_CODE=$STATE_OK
    fi
else
    # Something is not quite right :(
    echo -n "WARNING: Don't know the Sybase version"
    cat /tmp/$$.out.sybase
    exit $STATE_WARNING
fi

# We are okay unless we determined a warning or critical state earlier
[ $RET_CODE -eq $STATE_OK ] && echo "OK: Sybase $SYBASE_VERSION running ($SYBASE_DBNAME) | $PERFDATA"
[ -s /tmp/$$.conninfo ] && cat /tmp/$$.conninfo

# We are done
exit $RET_CODE
