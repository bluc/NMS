#!/bin/bash
################################################################
# (c) Copyright 2018 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# Based on https://www.icinga.com/docs/icinga2/latest/doc/12-icinga2-api/#icinga2-api-config-objects-delete

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

# Make sure we remove temporary files at exit
trap "rm -f /tmp/$$*" EXIT

#--------------------------------------------------------------------
# Get the credentials from /etc/icinga2/conf.d/api-users.conf
# Requires that "icinga2 api setup" has been run
if [ ! -s /etc/icinga2/conf.d/api-users.conf ]
then
    echo "Please run 'icinga2 api setup' first"
    exit 1
elif [ -z "$(grep root /etc/icinga2/conf.d/api-users.conf)" ]
then
    echo "Please run 'icinga2 api setup' first"
    exit 1
fi

cat << EOT > /tmp/$$.pl
#!/usr/bin/perl
open my \$in, '<', '/etc/icinga2/conf.d/api-users.conf' or die \$!;
my \$RootUser = 0;
while(<\$in>)
{   
    # Detect stanza for "root"
    \$RootUser = 1 if (/ApiUser\s*"root/o);
    if (\$RootUser)
    {
        my (\$key, \$val) = \$_ =~ /(.*?)=(.*)/;
        if (\$key =~ /password/o)
        {
            # Trim the "key"
            \$key =~ s/^\s*//;\$key =~ s/\s*\$//;
            # Trim the "val"
            \$val =~ s/^\s*//; \$val =~ s/\s*\$//; \$val =~ s/"//g; \$val =~ s/,//g;
            # Save the data
            \$data{\$key} = \$val;
            last;
        }
    }
}
# Show what we need
print 'ROOT_PW=', \$data{'password'}, "\n";
exit (0);
__END__
EOT
export $(perl /tmp/$$.pl)

#--------------------------------------------------------------------
# Get the status of Icinga2
wget -q --no-check-certificate --user=root --password=$ROOT_PW -O - \
 'https://localhost:5665/v1/status' | python -m json.tool

#--------------------------------------------------------------------
# Get an object (example for local host)
wget -q --no-check-certificate --user=root --password=$ROOT_PW -O - \
 --header='Accept: application/json' --method=GET \
 'https://localhost:5665/v1/objects/hosts/'$(hostname -s)'?cascade=1' | \
 python -m json.tool

#--------------------------------------------------------------------
# Delete an object
#wget -q --no-check-certificate --user=root --password=$ROOT_PW -O - \
# --header='Accept: application/json' --method=DELELETE \
# 'https://localhost:5665/v1/objects/hosts/devserver.bytronics.com?cascade=1' | \
# python -m json.tool

# We are done
exit 0
