#!/bin/bash

if [ $# -gt 0 ]
then
    REMOTE_IP="$1"
else
    read -p 'IP address of remote host: ' REMOTE_IP
    [ -z "$REMOTE_IP" ] && exit 1
fi

# Copy the SSH key to the remote server
ssh-copy-id -i /var/lib/nagios/.ssh/id_nms \
  -o KexAlgorithms=+diffie-hellman-group1-sha1 \
  -o HostKeyAlgorithms=+ssh-dss nms@$REMOTE_IP

# Add remote host to "known hosts" for nagios
sudo -u nagios ssh -i /var/lib/nagios/.ssh/id_nms \
  -o KexAlgorithms=+diffie-hellman-group1-sha1 \
  -o HostKeyAlgorithms=+ssh-dss nms@$REMOTE_IP \
    'echo "just a second ...";sleep 2;exit'

REMOTE_NAME=$(host $REMOTE_IP | awk '/arpa/{print $NF}' | sed 's/\.$//')
[[ $REMOTE_NAME =~ NXDOMAIN ]] && read -p "Name for $REMOTE_IP ?" REMOTE_NAME
if [ ! -z "$REMOTE_NAME" ]
then
    REMOTE_DOMAIN=${REMOTE_NAME#*.}
    if [ ! -s /etc/icinga2/conf.d/${REMOTE_NAME}.conf ]
    then
        # Set up a new host definition for icinga2
        cat << EOT > /etc/icinga2/conf.d/${REMOTE_NAME}.conf

object Host "${REMOTE_NAME}" {
  display_name = "${REMOTE_NAME}"
  /* Import the default host template defined in "templates.conf". */
  import "generic-host"
  import "unix-host"

  /* Specify the address attributes for checks e.g. "ssh" or "http". */
  address = "${REMOTE_IP}"

  /* Set custom attribute "os" for hostgroup assignment in "groups.conf". */
  vars.os = "SunOS"
  vars.domain = "${REMOTE_DOMAIN}"

  vars.notification["mail"] = {
    /* The UserGroup "icingaadmins" is defined in "users.conf". */
    groups = [ "icingaadmins" ]
  }
}
EOT
    fi
fi

# Some parting words
cat << EOT
The configuration file '/etc/icinga2/conf.d/${REMOTE_NAME}.conf'
exists - please review it.
EOT
read -t 3 -p 'Press ENTER to continue (or wait 3 seconds)' E

# We are done
exit 0
