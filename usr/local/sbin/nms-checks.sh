#!/bin/bash
#--------------------------------------------------------------------
# (c) CopyRight 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#--------------------------------------------------------------------

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

# Only run in multi-user mode
runlevel > /tmp/runlevel 2> /dev/null
[ -z "$(grep '^[A-Z0-9]' /tmp/runlevel)" ] && exit 0
[ $(who -r | awk '{print $2}') -ge 2 ] || exit 0

#-------------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE
            
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

# Globals
set $(date "+%_H %_M")
CURHOUR=$1
CURMIN=$2
UPTIME=$(cut -d. -f1 < /proc/uptime)

THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

if [ $UPTIME -gt 500 ]
then
    RESTART_ICINGA=''

    # Check whether we have new or changed
    #  or deleted files for icinga2
    sha256sum /etc/icinga2/conf.d/*conf > /tmp/$$.icinga2.checksums
    diff -wu /tmp/$$.icinga2.checksums /var/tmp/icinga2.checksums &> /dev/null
    if [ $? -ne 0 ]
    then
        # Reload icinga2 since we have some changes
        cat /tmp/$$.icinga2.checksums > /var/tmp/icinga2.checksums
        RESTART_ICINGA='reload'
    fi

    # Create a list of locally maintained users
    cat << EOT > /tmp/$$.bluc_users.conf
/*
 * B-LUC specific users (used for notifications)
 */


/**
 * B-LUC specific users
 */

object User "tbulli" {
  import "generic-user"
  display_name = "Thomas Bullinger"
  groups = [ "icingaadmins" ]
  email = "consult@btoy1.net"
  states = [ OK, Warning, Critical, Up, Down ]
  types = [ Problem, Acknowledgement, Recovery, Custom ]
}

object UserGroup "oncallusers" {
  display_name = "On call personnel"
}

EOT
    diff -wu /tmp/$$.bluc_users.conf /etc/icinga2/conf.d/bluc_users.conf &> /dev/null
    if [ $? -ne 0 ]
    then
        cat /tmp/$$.bluc_users.conf > /etc/icinga2/conf.d/bluc_users.conf
        RESTART_ICINGA='reload'
    fi

    # Create a list of locally maintained templates
    cat << EOT > /tmp/$$.bluc_templates.conf
/*
 * B-LUC specific templates
 */

/**
 * Provides default settings for UNIX hosts.
 */
template Host "unix-host" {
  vars.conn_program = "/usr/bin/ssh"
  vars.ssh_ipv4 = "1"

  check_command = "ssh"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for SNMP hosts.
 */
template Host "snmp-host" {
  vars.conn_program = "/usr/bin/snmpget"
  vars.snmp_address = "\$address\$"
  vars.snmp_community = "nms"
  vars.snmp_version = "1"
  vars.snmp_oid = ".1.3.6.1.2.1.1.1.0"
  check_command = "snmp"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for Windows hosts.
 */
template Host "windows-host" {
  vars.conn_program = "/bin/wmic"
  vars.os = "Windows"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for customer-facing web servers.
 */
template Host "webserver-external" {

  // This is needed for correct notification
  vars.externalweb = "1"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for database servers.
 */
template Host "dbserver" {

  // This is needed for correct notification
  vars.dbserver = "1"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for mail servers.
 */
template Host "mailserver" {

  // This is needed for correct notification
  vars.mailserver = "1"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for NFS servers.
 */
template Host "nfsserver" {

  // This is needed for correct notification
  vars.nfsserver = "1"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/**
 * Provides default settings for GIS servers.
 */
template Host "gisserver" {

  // This is needed for correct notification
  vars.gisserver = "1"

  // Allow for longer round-trip averages
  vars.ping_wrta = 150
  vars.ping_crta = 200

  // Allow for more packet losses
  vars.ping_wpl = 10
  vars.ping_cpl = 15
}

/*
 * Don't send notifications for services on a host that is down
 * (might need some more testing, esp. for the attribute "parent_host_name")
 * See https://www.icinga.com/docs/icinga2/latest/doc/03-monitoring-basics/#implicit-dependencies-for-services-on-host
 */
apply Dependency "disable-host-service-notifications" to Service {
  disable_notifications = true
  assign where true
}

/*
 * B-LUC specific notificiations.
 */

apply Notification "WebProblem" to Service {
  command = "mail-service-notification"
  user_groups = [ "oncallusers", "icingaadmins" ]
  interval = 1m // max. interval after delay
  times.begin = 2m // delay notification window
  assign where host.vars.externalweb == "1"
}

apply Notification "DatabaseProblem" to Service {
  command = "mail-service-notification"
  user_groups = [ "dbadmins", "icingaadmins" ]
  assign where host.vars.dbserver == "1"
}

apply Notification "GISProblem" to Service {
  command = "mail-service-notification"
  user_groups = [ "gisadmins", "icingaadmins" ]
  assign where host.vars.gisserver == "1"
}

/*
 * B-LUC specific global service definitions.
 */

apply Service "bluc_check_hardware" {
    import "generic-service"
    check_command = "bluc_check_hardware"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
    // No notifications
    enable_notifications = "0"

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux" || host.vars.os == "Fortigate" || host.vars.os == "Cisco"
}

apply Service "bluc_check_software" {
    import "generic-service"
    check_command = "bluc_check_software"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
    // No notifications
    enable_notifications = "0"

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux" || host.vars.os == "Fortigate" || host.vars.os == "Cisco"
}

apply Service "bluc_check_cpu" {
    import "generic-service"
    check_command = "bluc_check_cpu"
    vars.cpu_warnlevel = 70
    vars.cpu_critlevel = 90

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux" || host.vars.os == "Fortigate" || host.vars.os == "Cisco"
}

apply Service "bluc_check_load" {
    import "generic-service"
    check_command = "bluc_check_load"
    if (host.vars.os == "Fortigate" || host.vars.os == "Cisco") {
        // Allow higher loads for firewalls 
        vars.load_warnlevel = 75
        vars.load_critlevel = 90
    } else {
        // Standard servers
        vars.load_warnlevel = 15
        vars.load_critlevel = 30
    }

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux" || host.vars.os == "Fortigate" || host.vars.os == "Cisco"
}

apply Service "bluc_check_ifstat" {
    import "generic-service"
    check_command = "bluc_check_ifstat"

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux" || host.vars.os == "Fortigate" || host.vars.os == "Cisco"
}

apply Service "bluc_check_mem" {
    import "generic-service"
    check_command = "bluc_check_mem"
    if (host.vars.os == "SunOS" ) {
        // Allow higher levels for Solaris servers
        vars.swap_warnlevel = 85
        vars.swap_critlevel = 92
    } else {
        vars.swap_warnlevel = 55
        vars.swap_critlevel = 75
    }

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux"
}

apply Service "bluc_check_disk" {
    import "generic-service"
    check_command = "bluc_check_disk"
    vars.crps_warnlevel = 90
    vars.crps_critlevel = 95

    // Use the generic event handler
    event_command = "bluc_event_handler"
    vars.event_handler_command = "remove_old_files;\$service_name\$;\$service.state\$;\$service.state_type\$;\$service.check_attempt\$"

    assign where host.vars.os == "SunOS" || host.vars.os == "Linux"
}

apply Service "bluc_check_diskio_problems" {
    import "generic-service"
    check_command = "bluc_check_diskio_problems"

    assign where host.vars.os == "SunOS"
}

apply Service "tcp_sybase" {
    import "generic-service"
    check_command = "tcp"
    vars.tcp_ipv4 = "1"
    vars.tcp_port = "2638"
    // These times are in seconds!!!
    vars.tcp_wtime = "1"
    vars.tcp_ctime = "3"

    assign where host.vars.dbserver == "1"
}

apply Service "bluc_check_sybase" {
    import "generic-service"
    check_command = "bluc_check_sybase"
    // No notifications
    enable_notifications = "0"

    // Use the generic event handler
    event_command = "bluc_event_handler"
    vars.event_handler_command = "restart_sybase;\$service_name\$;\$service.state\$;\$service.state_type\$;\$service.check_attempt\$"

    assign where host.vars.dbserver == "1"
}

apply Service "http" {
    import "generic-service"
    vars.http_expect_body_eregi = "WHATEVER" // ADAPT!!!
    vars.http_extendedperfdata = "1"
    // Follow redirects
    vars.onredirect = "follow"
    // Disable HTTPS checking
    vars.http_ssl           = "0"
    // Time till warning is issued
    vars.http_warn_time     = "5"
    // Time till critical notification
    vars.http_critical_time = "10"
    check_command           = "http"

    // Use the generic event handler
    event_command = "bluc_event_handler"

    assign where host.vars.externalweb == "1"
}

apply Service "http_db_connection" {
    import "generic-service"
    vars.http_uri = "WHATEVER" // ADAPT!!!
    vars.http_expect_body_eregi = "WHATEVER" // ADAPT!!!
    vars.http_warn_time     = "5"
    vars.http_critical_time = "10"
    check_command           = "http"

    // Use the generic event handler
    event_command = "bluc_event_handler"
    vars.event_handler_command = "restart_webserver;\$service_name\$;\$service.state\$;\$service.state_type\$;\$service.check_attempt\$"

    assign where host.vars.externalweb == "1"
}

apply Service "bluc_check_nfs" {
    import "generic-service"
    check_command = "bluc_check_nfs"

    assign where host.vars.nfsserver == "1"
}

apply Service "smtp" {
    import "generic-service"
    check_command = "smtp"

    assign where host.vars.mailserver == "1"
}

apply Service "bluc_check_process_postfix" {
    import "generic-service"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "postfix.*master"
    vars.proc_min = 1
    vars.proc_max = 2

    // Use the generic event handler
    event_command = "bluc_event_handler"
    vars.event_handler_command = "restart_postfix;\$service_name\$;\$service.state\$;\$service.state_type\$;\$service.check_attempt\$"

    assign where host.vars.mailserver == "1"
}

apply Service "bluc_check_process_clamd" {
    import "generic-service"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "clamd"
    vars.proc_min = 1
    vars.proc_max = 2

    // Use the generic event handler
    event_command = "bluc_event_handler"
    vars.event_handler_command = "restart_clamd;\$service_name\$;\$service.state\$;\$service.state_type\$;\$service.check_attempt\$"

    assign where host.vars.mailserver == "1"
}

apply Service "bluc_check_postfix" {
    import "generic-service"
    check_command = "bluc_check_postfix"
    // check every 5 minutes (expressed in seconds)
    check_interval = 300
    vars.proc_max = 2

    assign where host.vars.mailserver == "1"
}
EOT
    diff -wu /tmp/$$.bluc_templates.conf /etc/icinga2/conf.d/bluc_templates.conf &> /dev/null
    if [ $? -ne 0 ]
    then
        cat /tmp/$$.bluc_templates.conf > /etc/icinga2/conf.d/bluc_templates.conf
        RESTART_ICINGA='reload'
    fi

    # Create a list of locally maintained commands
    cat << EOT > /tmp/$$.bluc_commands.conf
/**
 * Locally maintained commands
 */

// Generic event handler
object EventCommand "bluc_event_handler" {
  import "plugin-event-command"
  command = "/usr/bin/test \$service.check_attempt\$ -eq 2 && /usr/bin/test \$service.state\$ = 'CRITICAL' -o \$service.state\$ = 'WARNING' && /usr/local/nagios-plugins/bluc_event_handler -H \$address\$ -O \$os\$ -e \$conn_program\$ -t \$conn_timeout\$ -R \$bluc_proxy\$ -X \$service.name\$:\$service.state\$:\$service.state_type\$:\$service.check_attempt\$"
  // Allow up to 10 minutes execution time
  timeout = 600
}

// Command Declaration Block
// Service: Get download and upload speeds
object CheckCommand "bluc_speedtest" {
  command = [ "/usr/local/nagios-plugins/bluc_speedtest" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get UPS status
object CheckCommand "bluc_ups_status" {
  command = [ "/usr/local/nagios-plugins/bluc_ups_status" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get hardware information (where possible)
object CheckCommand "bluc_check_hardware" {
  command = [ "/usr/local/nagios-plugins/bluc_check_hardware" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get software information (where possible)
object CheckCommand "bluc_check_software" {
  command = [ "/usr/local/nagios-plugins/bluc_check_software" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Check partition usage (space and inodes)
object CheckCommand "bluc_check_disk" {
  command = [ "/usr/local/nagios-plugins/bluc_check_disk" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-w" = "\$crps_warnlevel\$"
    "-c" = "\$crps_critlevel\$"
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Check for disk I/O problems
object CheckCommand "bluc_check_diskio_problems" {
  command = [ "/usr/local/nagios-plugins/bluc_check_diskio_problems" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Check cpu "busyness"
object CheckCommand "bluc_check_cpu" {
  command = [ "/usr/local/nagios-plugins/bluc_check_cpu" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-w" = "\$cpu_warnlevel\$"
    "-c" = "\$cpu_critlevel\$"
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Check server load
object CheckCommand "bluc_check_load" {
  command = [ "/usr/local/nagios-plugins/bluc_check_load" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-w" = "\$load_warnlevel\$"
    "-c" = "\$load_critlevel\$"
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Check memory and swap usage (and alert of swap usage)
object CheckCommand "bluc_check_mem" {
  command = [ "/usr/local/nagios-plugins/bluc_check_mem" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-w" = "\$swap_warnlevel\$"
    "-c" = "\$swap_critlevel\$"
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get interface stats and throughput
object CheckCommand "bluc_check_ifstat" {
  command = [ "/usr/local/nagios-plugins/bluc_check_ifstat" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get service/process status
object CheckCommand "bluc_check_process" {
  command = [ "/usr/local/nagios-plugins/bluc_check_process" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-m" = "\$proc_min\$"
    "-M" = "\$proc_max\$"
    "-X" = "\$proc_name\$"
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get sybase status
object CheckCommand "bluc_check_sybase" {
  command = [ "/usr/local/nagios-plugins/bluc_check_sybase" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get NFS availability
object CheckCommand "bluc_check_nfs" {
  command = [ "/usr/local/nagios-plugins/bluc_check_nfs" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get VPN status
object CheckCommand "bluc_check_vpn" {
  command = [ "/usr/local/nagios-plugins/bluc_check_vpn" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-m" = "\$vpn_min\$"
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get load balancer status
object CheckCommand "bluc_check_bigip" {
  command = [ "/usr/local/nagios-plugins/bluc_check_bigip" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
    "-R" = {
      value = "\$bluc_proxy\$"
      description = "Sets the proxy information (proxy_ip:nat_ip:nat_type). Defaults to 'empty'."
    }
  }
}

// Command Declaration Block
// Service: Get possible postfix problems
object CheckCommand "bluc_check_postfix" {
  command = [ "/usr/local/nagios-plugins/bluc_check_postfix" ]
  arguments = {
    "-H" = {
      value = "\$address\$"
      description = "Host name, IP Address, or unix socket (must be an absolute path)."
    }
    "-O" = {
      value = "\$os\$"
      description = "Operating system (SunOS, Linux, Windows, Fortigate, Cisco, bigip)."
    }
    "-e" = {
      value = "\$conn_program\$"
      description = "Connection program (ssh, snmpget, wmic)."
    }
  }
}
EOT
    diff -wu /tmp/$$.bluc_commands.conf /etc/icinga2/conf.d/bluc_commands.conf &> /dev/null
    if [ $? -ne 0 ]
    then
        cat /tmp/$$.bluc_commands.conf > /etc/icinga2/conf.d/bluc_commands.conf
        RESTART_ICINGA='reload'
    fi

    # Rebuild the groups for icinga
    cat << EOT > /tmp/$$.groups.conf
/**
 * Host groups
 */
EOT

    grep -h 'vars.os' /etc/icinga2/conf.d/*conf | awk -F\" '{print $(NF-1)}' | sort -u | while read OS
    do
        cat << EOT > /tmp/$$.groups.conf
object HostGroup "${OS}-servers" {
  display_name = "$OS Servers"

  assign where host.vars.os == "$OS"
}
EOT
    done

    grep -h domain /etc/icinga2/conf.d/*conf | grep -v assign | sort -u | awk '{gsub(/"/,"");print $NF}' | while read DOMAIN
    do
        cat << EOT >> /tmp/$$.groups.conf

object HostGroup "${DOMAIN}-servers" {
  display_name = "$DOMAIN Servers"

  assign where host.vars.domain == "$DOMAIN"
}
EOT
    done

    cat << EOT >> /tmp/$$.groups.conf

object HostGroup "external-web-servers" {
  display_name = "Externally Facing Web Servers"

  assign where host.vars.externalweb == "1"
}

object HostGroup "database-servers" {
  display_name = "Active Database Servers"

  assign where host.vars.dbserver == "1"
}

object HostGroup "mail-servers" {
  display_name = "Mail Servers"

  assign where host.vars.mailserver == "1"
}

object HostGroup "load-balancers" {
  display_name = "Load Balancers"

  assign where host.vars.os == "bigip"
}
EOT
    cat << EOT >> /tmp/$$.groups.conf

/**
 * Service groups
 */

object ServiceGroup "ping" {
  display_name = "Ping Checks"

  assign where match("ping*", service.name)
}

object ServiceGroup "getinfo" {
  display_name = "Get information from remote hosts"

  assign where regex("bluc_check_(soft|hard)ware\$", service.check_command)
}

object ServiceGroup "http" {
  display_name = "HTTP Checks"

  assign where match("http*", service.check_command)
}

object ServiceGroup "disk" {
  display_name = "Disk Checks"

  assign where regex("(bluc_check_)?disk*", service.check_command)
}

object ServiceGroup "load" {
  display_name = "CPU and server load checks"

  assign where regex("bluc_check_(cpu|load)", service.check_command)
}

object ServiceGroup "network" {
  display_name = "Network Checks"

  assign where match("bluc_check_if*", service.check_command)
}

object ServiceGroup "process" {
  display_name = "Process Checks"

  assign where match("bluc_check_process", service.check_command)
}

object ServiceGroup "memory" {
  display_name = "Memory and Swap Checks"

  assign where match("bluc_check_mem", service.check_command)
}

object ServiceGroup "VPN-checks" {
  display_name = "VPN Checks"

  assign where match("bluc_check_vpn", service.check_command)
}

EOT
    diff -wu /tmp/$$.groups.conf /etc/icinga2/conf.d/groups.conf &> /dev/null
    if [ $? -ne 0 ]
    then
        cat /tmp/$$.groups.conf > /etc/icinga2/conf.d/groups.conf
        RESTART_ICINGA='reload'
    fi

    # Check the SSL cert and recreate it if necessary
    SSL_ENDS=$(openssl x509 -in /etc/icinga2/pki/${THISHOST}.crt -noout -enddate | cut -d= -f2)
    if [ $(date '+%s' -d "$SSL_ENDS") -lt $(date '+%s' -d '+ 7 days') ]
    then
        # Certificate expires in less than a week - rebuild it
        cd /etc/icinga2/pki
        mv -f ${THISHOST}.key ${THISHOST}.key.old
        mv -f ${THISHOST}.csr ${THISHOST}.key.csr
        mv -f ${THISHOST}.crt ${THISHOST}.key.crt
        icinga2 pki new-cert --cn $THISHOST --key ${THISHOST}.key --cert ${THISHOST}.crt
        RESTART_ICINGA='reload'
    fi

    # Setup some housecleaning for the database
    if [ -z "$(grep cleanup /etc/icinga2/features-available/ido-mysql.conf)" ]
    then
        grep -v '^}' /etc/icinga2/features-available/ido-mysql.conf > /tmp/ido-mysql.conf
        cat << EOT >> /tmp/ido-mysql.conf
  // Housecleaning - see https://docs.icinga.com/icinga2/latest/doc/module/icinga2/chapter/object-types
  cleanup = {
    acknowledgements_age = 6m
    commenthistory_age = 6m
    contactnotifications_age = 1m
    contactnotificationmethods_age = 1m
    downtimehistory_age = 1m
    eventhandlers_age = 6m
    externalcommands_age = 6m
    flappinghistory_age = 6m
    hostchecks_age = 1m
    logentries_age = 6m
    notifications_age = 1m
    processevents_age = 6m
    statehistory_age = 6m
    servicechecks_age = 1m
    systemcommands_age = 1m
  }
}
EOT
        cat /tmp/ido-mysql.conf > /etc/icinga2/features-available/ido-mysql.conf
        RESTART_ICINGA='restart'
    fi

    # Restart or reload icinga is necessary
    if [ ! -z "$RESTART_ICINGA" ]
    then
        icinga2 daemon -C &> /tmp/$$.conftest
        if [ $? -eq 0 ]
        then
            service icinga2 $RESTART_ICINGA
        else
            echo "ERROR: Something is wrong with icinga2 configurations"
            cat /tmp/$$.conftest
        fi
    fi

    # Once a day update local MIBs (needed for SNMP)
    [ "T${CURHOUR}:${CURMIN}" = 'T2:2' ] && download-mibs &> /dev/null
fi

#-------------------------------------------------------------------------
# We are done
exit 0
