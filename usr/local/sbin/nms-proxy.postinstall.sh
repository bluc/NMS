#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

if [ "T$(uname -i)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 0
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Can we use a popup dialog program for questions?
USE_DIALOG=0
[ -x /bin/whiptail ] && USE_DIALOG=1

#--------------------------------------------------------------------
# Install some needed packages
apt-get update
[ -x /usr/bin/snmpget ] || apt-get -y install snmp
# Install "rcpbind" to get RPC infos
[ -x /usr/sbin/rpcinfo ] || apt-get install rpcbind

wget -q -O /tmp/wmic_1.0_amd64.deb \
   https://gitlab.com/bluc/AdminTools/raw/master/usr/local/src/wmic_1.0_amd64.deb
cmp /tmp/wmic_1.0_amd64.deb /usr/local/src/wmic_1.0_amd64.deb 2> /dev/null
[ $? -ne 0 ] && cat /tmp/wmic_1.0_amd64.deb > /usr/local/src/wmic_1.0_amd64.deb
dpkg -i /usr/local/src/wmic_1.0_amd64.deb

#-------------------------------------------------------------------------
# The setup for the "nms" account is copied from "SetupNMSAccount.sh"

#-------------------------------------------------------------------------
# Setup the nms account if its doesn't exist yet
getent passwd nms &> /dev/null
if [ $? -ne 0 ]
then
    useradd -s $BASH -c 'NMS Monitor' -m nms
else
    usermod -s $BASH -c 'NMS Monitor' nms
fi

#-------------------------------------------------------------------------
# Update/set the password
echo 'nms:b3kkajudd' | chpasswd

#-------------------------------------------------------------------------
# Create the necessary sudo definitions
if [ -d /etc/sudoers.d ]
then
    # Linux host
    cat << EOT > /etc/sudoers.d/NMS
nms ALL= NOPASSWD: /usr/sbin/dmidecode
EOT
    chmod 0440 /etc/sudoers.d/NMS
fi

#-------------------------------------------------------------------------
# Some parting words
cat  << EOT

To fully activate this host, please run these commands
  as "root" on the NMS server:

ssh-copy-id -i /var/lib/nagios/.ssh/id_nms nms@<host-ip>
sudo -u nagios ssh-copy-id -i /var/lib/nagios/.ssh/id_nms nms@<host-ip>

Also create the necessary host definition in /etc/icinga2/conf.d
on the NMS server.

This host's IP addresses are:
EOT
ifconfig -a
sleep 2
if [[ $THISDOMAIN =~ bytronics ]]
then
    cat << EOT

Also copy all known MIBs from GitLAB to /usr/local/etc.
EOT
fi

#--------------------------------------------------------------------
# We are done
exit 0
