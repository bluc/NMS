#!/bin/bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################
# URL: https://raw.githubusercontent.com/B-LUC/NMS/master/usr/local/sbin/nms.postinstall.sh

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Sanity checks
if [ -s /etc/lsb-release ]
then
    source /etc/lsb-release
    if [ "T${DISTRIB_ID^^}" != 'TUBUNTU' ]
    then
        echo 'This is not an Ubuntu Server'
        exit 0
    fi
else
    echo 'This is not an Ubuntu Server'
    exit 0
fi

if [ "T$(uname -i)" != 'Tx86_64' ]
then
    echo 'This is not a 64-bit server'
    exit 0
fi

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#--------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG='-q'
[[ $- = *x* ]] && DEBUG='-v'

#--------------------------------------------------------------------
# This host and domain
THISHOST=$(hostname)
[[ $THISHOST = *.* ]] || THISHOST=$(hostname -f)
THISDOMAIN=${THISHOST#*.}

#--------------------------------------------------------------------
# Can we use a popup dialog program for questions?
USE_DIALOG=0
[ -x /bin/whiptail ] && USE_DIALOG=1

#--------------------------------------------------------------------
# Install some pre-requisites
[ -z "$(apt-key list | grep Icinga)" ] && wget -qO - http://packages.icinga.com/icinga.key | apt-key add -
if [ ! -s /etc/apt/sources.list.d/icinga2.list ]
then
    cat << EOT > /etc/apt/sources.list.d/icinga2.list
deb http://packages.icinga.com/ubuntu icinga-$DISTRIB_CODENAME main
deb-src http://packages.icinga.com/ubuntu icinga-$DISTRIB_CODENAME main 
EOT
    apt-get update
fi

#--------------------------------------------------------------------
# Install the NMS program along with the MariaDB database server
apt-get install icinga2 mariadb-server icinga2-ido-mysql vim-icinga2
vim-addons install icinga2

#--------------------------------------------------------------------
# Get plugins and SNMP mibs
apt-get install nagios-plugins snmp-mibs-downloader

#--------------------------------------------------------------------
# Install the web frontend
apt-get install libapache2-mod-php7.0 icingaweb2

#--------------------------------------------------------------------
# Adapt PHP configuration
CUR_TZ=$(timedatectl | awk '/zone:/{sub(/\//,"\\/",$3);print $3}')
sed -i -e "s/;date.timezone =/date.timezone = $CUR_TZ/" /etc/php/7.0/apache2/php.ini

#--------------------------------------------------------------------
# Allow icingaweb2 access to web server data
addgroup --system icingaweb2
usermod -a -G icingaweb2 www-data
# Set the correct apache worker
a2dismod mpm_event
a2enmod mpm_prefork
service apache2 restart

#--------------------------------------------------------------------
# Adapt the default index.html" file
if [ -z "$(grep B-LUC /var/www/html/index.html)" ]
then
    cat << EOT > /var/www/html/index.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome</title>
 <meta name="copyright" content="B-LUC Consulting">
 <meta http-equiv="refresh" content="2;url=icingaweb2/">
</head>
<body>
 You will be redirected to the Icinga web frontend in two seconds. If
 you aren't forwarded to the it, please click <a href=icingaweb2/> here </a>.
</body>
</html>
EOT
fi

#--------------------------------------------------------------------
# Enable some features
icinga2 feature enable ido-mysql
if [ -z "$(grep cleanup /etc/icinga2/features-enabled/ido-mysql.conf)" ]
then
    # Add expirations for historical data
    sed 's/^}/\n\/\/ Keep data for max. 2 years\n  cleanup = {\n    acknowledgements_age = 720d\n    commenthistory_age = 720d\n    contactnotifications_age = 720d\n    contactnotificationmethods_age = 720d\n    downtimehistory_age = 720d\n    eventhandlers_age = 720d\n    externalcommands_age = 720d\n    flappinghistory_age = 720d\n    hostchecks_age = 720d\n    logentries_age = 720d\n    notifications_age = 720d\n    processevents_age = 720d\n    statehistory_age = 720d\n    servicechecks_age = 720d\n    systemcommands_age =  720d\n  }\n}/' /etc/icinga2/features-enabled/ido-mysql.conf
fi
#sed 's/CREATE TABLE/CREATE TABLE IF NOT EXISTS/' /usr/share/icinga2-ido-mysql/schema/mysql.sql | \
mysql --defaults-file=/etc/mysql/debian.cnf -f icinga2 < /usr/share/icinga2-ido-mysql/schema/mysql.sql

icinga2 feature enable command syslog livestatus statusdata graphite
wget --no-check-certificate -O /usr/local/bin/icinga2_status.pl \
  https://exchange.icinga.com/zhmurko/Services+in+text+console/files/1114/icinga2_status.pl
chmod 755 /usr/local/bin/icinga2_status.pl

#--------------------------------------------------------------------
# Restart icinga2 to make the features available
service icinga2 restart

#--------------------------------------------------------------------
# Install some decent mail programs so that notifications work
[ -x /usr/sbin/nullmailer-send ] || apt-get install nullmailer mailutils
# Install "sshpass" to support ssh logins with password
[ -x /usr/bin/sshpass ] || apt-get install sshpass
# Install "rcpbind" to get RPC infos
[ -x /usr/sbin/rpcinfo ] || apt-get install rpcbind

#--------------------------------------------------------------------
# This is needed for the web setup
icingacli setup config directory
mysql --defaults-file=/etc/mysql/debian.cnf << EOT
CREATE DATABASE IF NOT EXISTS icingaweb2;
GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE ON icingaweb2.* TO 'icingaweb2'@'localhost' IDENTIFIED BY 'icingaweb2';
FLUSH PRIVILEGES;
EOT
mysql --defaults-file=/etc/mysql/debian.cnf icingaweb2 < /usr/share/icingaweb2/etc/schema/mysql.schema.sql

#--------------------------------------------------------------------
# Setup a suitable hosts configuration file
if [ -z "$(grep BLUC /etc/icinga2/conf.d/hosts.conf)" ]
then
    cat << EOT > /etc/icinga2/conf.d/hosts.conf
// BLUC adaptations done
/*
 * Host definitions with object attributes
 * used for apply rules for Service, Notification,
 * Dependency and ScheduledDowntime objects.
 *
 * Tip: Use "icinga2 object list --type Host" to
 * list all host objects after running
 * configuration validation ("icinga2 daemon -C").
 */

/*
 * This is an example host based on your
 * local host's FQDN. Specify the NodeName
 * constant in "constants.conf" or use your
 * own description, e.g. "db-host-1".
 */

object Host NodeName {
  /* Import the default host template defined in "templates.conf". */
  import "generic-host"

  /* Specify the address attributes for checks e.g. "ssh" or "http". */
  address = "127.0.0.1"
//  address6 = "::1"

  /* Set custom attribute "os" for hostgroup assignment in "groups.conf". */
  vars.os = "Linux"

  /* Define http vhost attributes for service apply rules in "services.conf". */
  vars.http_vhosts["http"] = {
    http_uri = "/"
  }
  /* Uncomment if you've sucessfully installed Icinga Web 2. */
  vars.http_vhosts["Icinga Web 2"] = {
    http_uri = "/icingaweb2"
  }

  /* Define disks and attributes for service apply rules in "services.conf". */
  vars.disks["disk"] = {
    /* No parameters. */
  }
  vars.disks["disk /"] = {
    disk_partitions = "/"
  }
EOT
    [ -z "$(grep '/boot' /proc/mounts)" ] || cat << EOT >> /etc/icinga2/conf.d/hosts.conf
  vars.disks["disk /boot"] = {
    disk_partitions = "/boot"
  }
EOT

    cat << EOT >> /etc/icinga2/conf.d/hosts.conf
  // All but docker disks
  vars.disk_all = "1"
  vars.disk_ignore_ereg_path = "docker"

  /* Define notification mail attributes for notification apply rules in "notifications.conf". */
  vars.notification["mail"] = {
    /* The UserGroup "icingaadmins" is defined in "users.conf". */
    groups = [ "icingaadmins" ]
  }
}
EOT
fi

#--------------------------------------------------------------------
# Create a local plugins directory
mkdir -p /usr/local/nagios-plugins
chown -R nagios: /usr/local/nagios-plugins

#--------------------------------------------------------------------
# Create a ssh key for the "nms" account to be used for remote machines
NMS_HOME=$(getent passwd nagios | cut -d: -f6)
if [ ! -s ${NMS_HOME}/.ssh/id_nms.pub ]
then
    mkdir -p ${NMS_HOME}/.ssh
    ssh-keygen -t rsa -b 4096 -f ${NMS_HOME}/.ssh/id_nms -C nms
    chown -R nagios: ${NMS_HOME}/.ssh
    chmod 0644 ${NMS_HOME}/.ssh/id_nms.pub
    chmod 0400 ${NMS_HOME}/.ssh/id_nms
fi

#--------------------------------------------------------------------
# Install a GRAPHITE server (inside a docker image)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Install docker OS package
[ -x /usr/bin/docker ] || apt-get  install docker.io

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Pull the docker instance
docker pull sitespeedio/graphite

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Create a new directory path for all things graphite
mkdir -p /opt/graphite/{storage,conf,webapp}
mkdir -p /opt/graphite/webapp/graphite

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Create the necessary ".htpasswd" file
htpasswd -b -c /opt/graphite/.htpasswd igraphite icingaweb2

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Pull the graphite module for icingaweb2
wget --no-check-certificate -O /usr/local/src/icingaweb2-module-graphite.zip \
  https://github.com/findmypast/icingaweb2-module-graphite/archive/master.zip
cd /tmp
unzip -qo /usr/local/src/icingaweb2-module-graphite
mkdir -p /usr/share/icingaweb2/modules/graphite
cp -r icingaweb2-module-graphite-master/* /usr/share/icingaweb2/modules/graphite

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Create a suitable configuration for the docker image
# Based on https://www.zhmurko.com/services/icinga2-monitoring/icingaweb2-graphite-integration/
mkdir -p /etc/icingaweb2/modules/graphite
cat << EOT > /etc/icingaweb2/modules/graphite/config.ini
; See https://raw.githubusercontent.com/findmypast/icingaweb2-module-graphite/master/application/views/scripts/config/index.phtml
[graphite]
metric_prefix = icinga
base_url = http://igraphite:icingaweb2@127.0.0.1:8080/render?

remote_fetch = true
remote_verify_peer = false
remote_verify_peer_name = false

legacy_mode = false
; if legacy mode is false (2.4 and newer):
service_name_template = "icinga2.\$host.name\$.services.\$service.name\$.\$service.check_command\$.perfdata.\$metric\$.value"
host_name_template = "icinga2.\$host.name\$.host.\$host.check_command\$.perfdata.\$metric\$.value"

;this template is used for the small image, macros \$target\$ , \$areaMode\$ and \$areaAlpha\$ can used.
graphite_args_template = "&target=\$target\$&source=0&width=300&height=120&hideAxes=true&lineWidth=2&hideLegend=true&colorList=\$colorList\$&areaMode=\$areaMode\$&areaAlpha=\$areaAlpha\$&bgcolor=848383"
;this template is used for the large image, macros \$target\$ , \$areaMode\$ and \$areaAlpha\$ can used.
;graphite_large_args_template = "&target=alias(color(\$target\$_warn,'yellow'),'warning')&target=alias(color(\$target\$_crit,'red'),'critical')&target=\$target\$&source=0&width=800&height=700&colorList=\$colorList\$&lineMode=connected&areaMode=\$areaMode\$&areaAlpha=\$areaAlpha\$&bgcolor=848383"
graphite_iframe_w = 800px
graphite_iframe_h = 700px
graphite_area_mode = all
graphite_area_alpha = 0.1
graphite_summarize_interval = 10min
graphite_color_list = 049BAF,EE1D00,04B06E,0446B0,871E10,CB315D,B06904,B0049C
EOT

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Create a icingaweb2 specific storage schema
# Based on https://github.com/findmypast/icingaweb2-module-graphite
cat << EOT > /opt/graphite/conf/storage-schemas.conf
# Schema definitions for Whisper files. Entries are scanned in order,
# and first match wins. This file is scanned for changes every 60 seconds.
#
#  [name]
#  pattern = regex
#  retentions = timePerPoint:timeToStore, timePerPoint:timeToStore, ...

# Carbon's internal metrics. This entry should match what is specified in
# CARBON_METRIC_PREFIX and CARBON_METRIC_INTERVAL settings
[carbon]
pattern = ^carbon\\.
retentions = 60:1d

[icinga2_internals]
pattern = ^icinga2\..*\.(max_check_attempts|reachable|current_attempt|execution_time|latency|state|state_type)
retentions = 5m:7d

[icinga2_default]
pattern = ^icinga2\.
retentions =  1m:2d,5m:10d,30m:90d,360m:4y

[cath_them_all]
pattern = .*
retentions = 5m:1d,15m:30d
EOT

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Create a customized web configuration for graphite
# Based on https://graphite.readthedocs.io/en/latest/config-local-settings.html
cat << EOT > /opt/graphite/webapp/graphite/local_settings.py
## Graphite local_settings.py
# Edit this file to customize the default Graphite webapp settings
#
# Additional customizations to Django settings can be added to this file as well

#####################################
# General Configuration #
#####################################
# Set this to a long, random unique string to use as a secret key for this
# install. This key is used for salting of hashes used in auth tokens,
# CRSF middleware, cookie storage, etc. This should be set identically among
# instances if used behind a load balancer.
#SECRET_KEY = 'UNSAFE_DEFAULT'

# In Django 1.5+ set this to the list of hosts your graphite instances is
# accessible as. See:
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-ALLOWED_HOSTS
#ALLOWED_HOSTS = [ '*' ]

# Set your local timezone (Django's default is America/Chicago)
# If your graphs appear to be offset by a couple hours then this probably
# needs to be explicitly set to your local timezone.
TIME_ZONE = '$CUR_TZ'

# Override this to provide documentation specific to your Graphite deployment
#DOCUMENTATION_URL = "http://graphite.readthedocs.org/"

# Logging
#LOG_RENDERING_PERFORMANCE = True
#LOG_CACHE_PERFORMANCE = True
#LOG_METRIC_ACCESS = True

# Enable full debug page display on exceptions (Internal Server Error pages)
#DEBUG = True

# If using RRD files and rrdcached, set to the address or socket of the daemon
#FLUSHRRDCACHED = 'unix:/var/run/rrdcached.sock'

# This lists the memcached servers that will be used by this webapp.
# If you have a cluster of webapps you should ensure all of them
# have the *exact* same value for this setting. That will maximize cache
# efficiency. Setting MEMCACHE_HOSTS to be empty will turn off use of
# memcached entirely.
#
# You should not use the loopback address (127.0.0.1) here if using clustering
# as every webapp in the cluster should use the exact same values to prevent
# unneeded cache misses. Set to [] to disable caching of images and fetched data
#MEMCACHE_HOSTS = ['10.10.10.10:11211', '10.10.10.11:11211', '10.10.10.12:11211']
#DEFAULT_CACHE_DURATION = 60 # Cache images and data for 1 minute


#####################################
# Filesystem Paths #
#####################################
# Change only GRAPHITE_ROOT if your install is merely shifted from /opt/graphite
# to somewhere else
#GRAPHITE_ROOT = '/opt/graphite'

# Most installs done outside of a separate tree such as /opt/graphite will only
# need to change these three settings. Note that the default settings for each
# of these is relative to GRAPHITE_ROOT
#CONF_DIR = '/opt/graphite/conf'
#STORAGE_DIR = '/opt/graphite/storage'
#CONTENT_DIR = '/opt/graphite/webapp/content'

# To further or fully customize the paths, modify the following. Note that the
# default settings for each of these are relative to CONF_DIR and STORAGE_DIR
#
## Webapp config files
#DASHBOARD_CONF = '/opt/graphite/conf/dashboard.conf'
#GRAPHTEMPLATES_CONF = '/opt/graphite/conf/graphTemplates.conf'

## Data directories
# NOTE: If any directory is unreadable in DATA_DIRS it will break metric browsing
#WHISPER_DIR = '/opt/graphite/storage/whisper'
#RRD_DIR = '/opt/graphite/storage/rrd'
#DATA_DIRS = [WHISPER_DIR, RRD_DIR] # Default: set from the above variables
#LOG_DIR = '/opt/graphite/storage/log/webapp'
#INDEX_FILE = '/opt/graphite/storage/index'  # Search index file


#####################################
# Email Configuration #
#####################################
# This is used for emailing rendered Graphs
# Default backend is SMTP
#EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#EMAIL_HOST = 'localhost'
#EMAIL_PORT = 25
#EMAIL_HOST_USER = ''
#EMAIL_HOST_PASSWORD = ''
#EMAIL_USE_TLS = False
# To drop emails on the floor, enable the Dummy backend:
#EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'


#####################################
# Authentication Configuration #
#####################################
## LDAP / ActiveDirectory authentication setup
#USE_LDAP_AUTH = True
#LDAP_SERVER = "ldap.mycompany.com"
#LDAP_PORT = 389
#	OR
#LDAP_URI = "ldaps://ldap.mycompany.com:636"
#LDAP_SEARCH_BASE = "OU=users,DC=mycompany,DC=com"
#LDAP_BASE_USER = "CN=some_readonly_account,DC=mycompany,DC=com"
#LDAP_BASE_PASS = "readonly_account_password"
#LDAP_USER_QUERY = "(username=%s)"  #For Active Directory use "(sAMAccountName=%s)"
#
# If you want to further customize the ldap connection options you should
# directly use ldap.set_option to set the ldap module's global options.
# For example:
#
#import ldap
#ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
#ldap.set_option(ldap.OPT_X_TLS_CACERTDIR, "/etc/ssl/ca")
#ldap.set_option(ldap.OPT_X_TLS_CERTFILE, "/etc/ssl/mycert.pem")
#ldap.set_option(ldap.OPT_X_TLS_KEYFILE, "/etc/ssl/mykey.pem")
# See http://www.python-ldap.org/ for further details on these options.

## REMOTE_USER authentication. See: https://docs.djangoproject.com/en/dev/howto/auth-remote-user/
#USE_REMOTE_USER_AUTHENTICATION = True

# Override the URL for the login link (e.g. for django_openid_auth)
#LOGIN_URL = '/account/login'


##########################
# Database Configuration #
##########################
# By default sqlite is used. If you cluster multiple webapps you will need
# to setup an external database (such as MySQL) and configure all of the webapp
# instances to use the same database. Note that this database is only used to store
# Django models such as saved graphs, dashboards, user preferences, etc.
# Metric data is not stored here.
#
# DO NOT FORGET TO RUN 'manage.py syncdb' AFTER SETTING UP A NEW DATABASE
#
# The following built-in database engines are available:
#  django.db.backends.postgresql          # Removed in Django 1.4
#  django.db.backends.postgresql_psycopg2
#  django.db.backends.mysql
#  django.db.backends.sqlite3
#  django.db.backends.oracle
#
# The default is 'django.db.backends.sqlite3' with file 'graphite.db'
# located in STORAGE_DIR
#
#DATABASES = {
#    'default': {
#        'NAME': '/opt/graphite/storage/graphite.db',
#        'ENGINE': 'django.db.backends.sqlite3',
#        'USER': '',
#        'PASSWORD': '',
#        'HOST': '',
#        'PORT': ''
#    }
#}
#


#########################
# Cluster Configuration #
#########################
# (To avoid excessive DNS lookups you want to stick to using IP addresses only in this entire section)
#
# This should list the IP address (and optionally port) of the webapp on each
# remote server in the cluster. These servers must each have local access to
# metric data. Note that the first server to return a match for a query will be
# used.
#CLUSTER_SERVERS = ["10.0.2.2:80", "10.0.2.3:80"]

## These are timeout values (in seconds) for requests to remote webapps
#REMOTE_STORE_FETCH_TIMEOUT = 6   # Timeout to fetch series data
#REMOTE_STORE_FIND_TIMEOUT = 2.5  # Timeout for metric find requests
#REMOTE_STORE_RETRY_DELAY = 60    # Time before retrying a failed remote webapp
#REMOTE_FIND_CACHE_DURATION = 300 # Time to cache remote metric find results

## Remote rendering settings
# Set to True to enable rendering of Graphs on a remote webapp
#REMOTE_RENDERING = True
# List of IP (and optionally port) of the webapp on each remote server that
# will be used for rendering. Note that each rendering host should have local
# access to metric data or should have CLUSTER_SERVERS configured
#RENDERING_HOSTS = []
#REMOTE_RENDER_CONNECT_TIMEOUT = 1.0

# If you are running multiple carbon-caches on this machine (typically behind a relay using
# consistent hashing), you'll need to list the ip address, cache query port, and instance name of each carbon-cache
# instance on the local machine (NOT every carbon-cache in the entire cluster). The default cache query port is 7002
# and a common scheme is to use 7102 for instance b, 7202 for instance c, etc.
#
# You *should* use 127.0.0.1 here in most cases
#CARBONLINK_HOSTS = ["127.0.0.1:7002:a", "127.0.0.1:7102:b", "127.0.0.1:7202:c"]
#CARBONLINK_TIMEOUT = 1.0

#####################################
# Additional Django Settings #
#####################################
# Uncomment the following line for direct access to Django settings such as
# MIDDLEWARE_CLASSES or APPS
#from graphite.app_settings import *

import os
os.environ.setdefault('LANG','en_US')

LOG_DIR = '/var/log/graphite'
SECRET_KEY = '\$(date +%s | sha256sum | base64 | head -c 64)'
EOT
vi +/TIME_ZONE /opt/graphite/webapp/graphite/local_settings.py

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Create a script to (re)start the graphite server
cat << EOT > /usr/local/sbin/run-graphite
#!/bin/bash
mkdir -p /opt/graphite/storage/whisper/icinga2
RETCODE=1
while [ \$RETCODE -ne 0 ]
do
    docker -D run -d \\
      --name graphite \\
      -p 8080:80 \\
      -p 2003:2003 \\
      -v /opt/graphite/.htpasswd:/etc/nginx/.htpasswd \\
      -v /opt/graphite/storage/whisper:/opt/graphite/storage/whisper \\
      -v /opt/graphite/conf/storage-schemas.conf:/opt/graphite/conf/storage-schemas.conf \\
      -v /opt/graphite/webapp/graphite/local_settings.py:/opt/graphite/webapp/graphite/local_settings.py \\
      sitespeedio/graphite
    RETCODE=\$?
    [ \$RETCODE -ne 0 ] && docker rm -f graphite
done
EOT
chmod 0744 /usr/local/sbin/run-graphite

# And a cronjob to make sure
cat << EOT > /etc/cron.d/nms
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin
MAILTO=root@$THISDOMAIN
#===============================================================
# Keep the graphite docker running
* * * * * root [ -z "\$(ps -wefH | grep 'runsv graphit[e]')" ] && /usr/local/sbin/run-graphite
# Maintain the icinga configurations
* * * * * root [ -x /usr/local/sbin/nms-checks.sh ] && /usr/local/sbin/nms-checks.sh
# Clean up old mountpoints for docker images
2 2 * * * root [ -d /var/lib/docker/aufs/mnt ] && (cd /var/lib/docker/aufs/mnt; rmdir *) 2> /dev/null
EOT

wget -q -O /tmp/wmic_1.0_amd64.deb \
   https://gitlab.com/bluc/AdminTools/raw/master/usr/local/src/wmic_1.0_amd64.deb
cmp /tmp/wmic_1.0_amd64.deb /usr/local/src/wmic_1.0_amd64.deb 2> /dev/null
[ $? -ne 0 ] && cat /tmp/wmic_1.0_amd64.deb > /usr/local/src/wmic_1.0_amd64.deb
dpkg -i /usr/local/src/wmic_1.0_amd64.deb

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Show some parting words
LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
icingacli setup token create
SETUP_TOKEN=$(icingacli setup token show)
WEBUSER_PW=$(openssl passwd -1 icingaweb2)
cat << EOT
Point your browser here if you haven't done the initial setup yet:
http://$LOCALIP/icingaweb2/setup

Use this "Setup Token": $SETUP_TOKEN

Use these login credentials (you can change them later):
Database for web interface:  icingaweb2/icingaweb2
Database for core engine  :  icinga2/icinga

You might have to create the main icinga2 web user like this:
mysql --defaults-file=/etc/mysql/debian.cnf icingaweb2 << EOS
INSERT INTO icingaweb_user (name, active, password_hash) VALUES ('iadmin', 1, '$WEBUSER_PW')
EOS
EOT

#--------------------------------------------------------------------
#DIRECTOR## Install the director module (not fully tested)
#DIRECTOR## As per https://github.com/Icinga/icingaweb2-module-director/blob/master/doc/02-Installation.md
#DIRECTOR#
#DIRECTOR## Create the database for the director module with a suitable password
#DIRECTOR#DB_PASS='m2phAs3r'
#DIRECTOR#API_PASS='mk3phAs3r'
#DIRECTOR#mysql --defaults-file=/etc/mysql/debian.cnf << EOT
#DIRECTOR#CREATE DATABASE IF NOT EXISTS director CHARACTER SET 'utf8';
#DIRECTOR#GRANT ALL ON director.* TO director@localhost IDENTIFIED BY '$DB_PASS';
#DIRECTOR#EOT
#DIRECTOR#
#DIRECTOR## Download and install the director module
#DIRECTOR#mkdir -p /tmp/director
#DIRECTOR#wget -qO /tmp/director/director.zip \
#DIRECTOR#  https://github.com/Icinga/icingaweb2-module-director/archive/master.zip
#DIRECTOR#cd /tmp/director
#DIRECTOR#unzip -qo director.zip
#DIRECTOR#ls -sla
#DIRECTOR#mkdir -p /usr/share/icingaweb2/modules/director
#DIRECTOR#cp -a icingaweb2-module-director-master/* /usr/share/icingaweb2/modules/director
#DIRECTOR#
#DIRECTOR## Setup the API module for icinga2
#DIRECTOR## See https://github.com/Icinga/icingaweb2-module-director/blob/master/doc/04-Getting-started.md
#DIRECTOR#icinga2 api setup
#DIRECTOR#if [ -z "$(grep director /etc/icinga2/conf.d/api-users.conf)" ]
#DIRECTOR#then
#DIRECTOR#    # Create a new api user for the director module
#DIRECTOR#    cat << EOT >> /etc/icinga2/conf.d/api-users.conf
#DIRECTOR#object ApiUser "director" {
#DIRECTOR#  password = "$API_PASS"
#DIRECTOR#  permissions = [ "*" ]
#DIRECTOR#  //client_cn = ""
#DIRECTOR#}
#DIRECTOR#EOT
#DIRECTOR#   cat << EOT
#DIRECTOR#
#DIRECTOR#To allow the configuration of an API user your Icinga 2 instance needs a zone
#DIRECTOR#and an endpoint object for itself.  If you have a clustered setup or you are
#DIRECTOR#using agents you already have this.  If you are using a fresh Icinga 2
#DIRECTOR#installation or a standalone setup with other ways of checking your clients,
#DIRECTOR#you will have to create them.
#DIRECTOR#
#DIRECTOR#The easiest way to set up Icinga 2 with a zone and endpoint is by running
#DIRECTOR#the Icinga 2 Setup Wizard.
#DIRECTOR#EOT
#DIRECTOR#fi
#DIRECTOR#
#DIRECTOR## Some parting words
#DIRECTOR#cat << EOT
#DIRECTOR#In your Icinga2 web frontend go to Configuration->Application->aResources
#DIRECTOR# and create a new database resource pointing to your newly created database. 
#DIRECTOR#
#DIRECTOR#=> The database name is 'director'.
#DIRECTOR#=> The database user is 'director'.
#DIRECTOR#=> The database password is '$DB_PASS'.
#DIRECTOR#=> Ensure that you choose 'utf8' as an encoding.
#DIRECTOR#
#DIRECTOR#The enable the module in Configuration->Module->director and configure
#DIRECTOR# it to use the newly create resource.
#DIRECTOR#
#DIRECTOR#Lastly, you might have to fo through the kickstarter wizard to import
#DIRECTOR# existing configurations.
#DIRECTOR#
#DIRECTOR#=> The API user is 'director'.
#DIRECTOR#=> The API password is '$API_PASS'.
#DIRECTOR#EOT

#--------------------------------------------------------------------
# We are done
exit 0
