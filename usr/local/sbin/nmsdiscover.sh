#!/usr/bin/env bash
################################################################
# (c) Copyright 2017 B-LUC Consulting and Thomas Bullinger
#
# Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
################################################################

# Either not an Icinga2 server or missing host configurations
[ -s /etc/icinga2/conf.d/hosts.conf ] || exit 0

#--------------------------------------------------------------------
# Set a sensible path for executables
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PROG=${0##*/}

#--------------------------------------------------------------------
# Ensure that only one instance is running
LOCKFILE=/tmp/$PROG.lock
if [ -f $LOCKFILE ]
then
    # The file exists so read the PID
    MYPID=$(< $LOCKFILE)
    [ -z "$(ps h -p $MYPID)" ] || exit 0
fi

# Make sure we remove the lock file at exit
trap "rm -f $LOCKFILE /tmp/$$*" EXIT
echo "$$" > $LOCKFILE            

#-------------------------------------------------------------------
# Specifying "-x" to the bash invocation = DEBUG
DEBUG=''
[[ $- = *x* ]] && DEBUG='-v'

# Function to convert netmasks into CIDR notation
# See: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
function mask2cdr ()
{
   # Assumes there's no "255." after a non-255 byte in the mask
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}

# Install nmap if necessary
[ -x /usr/bin/nmap ] || apt-get -y install nmap

# Get local network parameters
LOCALIF=$(ip route get 8.8.8.8 | awk '/via/{print $5; exit}')
LOCALIP=$(ifconfig $LOCALIF | sed -n "s/.*inet addr:\([0-9.]*\).*/\1/p")
LOCALMASK=$(ifconfig $LOCALIF | sed -n -e 's/.*Mask:\(.*\)$/\1/p')
# From: http://www.routertech.org/viewtopic.php?t=1609
l="${LOCALIP%.*}";r="${LOCALIP#*.}";n="${LOCALMASK%.*}";m="${LOCALMASK#*.}"
LOCALNET=$((${LOCALIP%%.*}&${LOCALMASK%%.*})).$((${r%%.*}&${m%%.*})).$((${l##*.}&${n##*.})).$((${LOCALIP##*.}&${LOCALMASK##*.}))
CIDRMASK=$(mask2cdr $LOCALMASK)

# Discover hosts that are not yet defined
INPUT_LIST="$1"
if [ -z "$INPUT_LIST" ]
then
    echo "Generating input list with IP addresses in the local network $LOCALNET/$CIDRMASK"
    nmap $DEBUG -n -sn -oG /tmp/$$.localhosts $LOCALNET/$CIDRMASK &> /dev/null
    [ -z "$DEBUG" ] || less /tmp/$$.localhosts
    awk '/Up/{print $2}' /tmp/$$.localhosts > /tmp/$$.hosts
    [ -z "$DEBUG" ] || less /tmp/$$.hosts
    INPUT_LIST=/tmp/$$.hosts
fi
if [ ! -s $INPUT_LIST ]
then
    echo "Input list doesn't contain any IP address"
    exit 0
fi

# Script to server info
cat << EOT > /tmp/$$.getinfo
#!/usr/bin/env bash
OS=\$(uname -s)
echo "OS=\$OS"
if [ "T\$OS" = 'TLinux' ]
then
    netstat -nta | awk '/:80.*LISTEN/{print "HTTPS="\$0}'
    netstat -nta | awk '/:443.*LISTEN/{print "HTTPS="\$0}'
elif [ "T\$OS" = 'TSunOS' ]
then
    netstat -n -P tcp | egrep -v '(dgram|stream)' | awk '/\.80 /{print "HTTP="\$0}'
    netstat -n -P tcp | egrep -v '(dgram|stream)' | awk '/\.443 /{print "HTTPS="\$0}'
fi
echo "All running processes:"
ps -eo comm,args
EOT
NMS_HOME=$(getent passwd nagios | cut -d: -f6)
rm -f /tmp/SSH_Hosts /tmp/Windows_Hosts
RESTART_ICINGA2=0
while read HOST_IP
do
echo "HOST_IP = '$HOST_IP'"
    # Skip any host that is already in any of Icinga's host files
    if [ ! -z "$(grep -wh address.=..$HOST_IP /etc/icinga2/conf.d/*.conf)" ]
    then
        echo "Host '$HOST_IP' already defined"
        sleep 3
        continue
    fi

    # Check whether host is reachable via SSH or Windows-RPC
    rm -f /tmp/$$.ssh
    echo "Checking '$HOST_IP' for SSH, Telnet, MSRPC and SNMP connectivity"
    nmap $DEBUG -n -p 22,23,135,161 -oG /tmp/$$.connectivity $HOST_IP &> /dev/null
    
    # Skip any host that can't be reached via SSH
    if [ ! -z "$(grep 'Ports: 22/open' /tmp/$$.connectivity)" ]
    then
        HOST_NAME=$(host $HOST_IP | awk '{print $NF}' | awk -F\. 'NF>3{sub(/.$/,"");print}')
        if [ -z "$HOST_NAME" ]
        then
            cat << EOT

Host '$HOST_IP' doesn't have a reverse DNS entry,
 please fix that and rerun this script.
EOT
            sleep 3
            continue
        fi
        echo "$HOST_IP" >> /tmp/SSH_Hosts

        # Check whether host is a Fortigate firewall
        HOST_VENDOR=$(GetMacVendor.sh $HOST_IP)
        if [[ $HOST_VENDOR =~ Fortinet ]]
        then
            #*************************************#
            # *** Define a Fortigate firewall *** #
            #*************************************#
            HOST_OS='Fortigate'

            # See https://zyradyl.github.io/2015/08/16/Icinga2-Tutorial-Part-2/
            cat << EOT > /etc/icinga2/conf.d/${HOST_NAME}.conf

object Host "$HOST_NAME" {
  display_name = "$HOST_NAME"
  /* Import the default host template defined in "templates.conf". */
  import "generic-host"
  import "snmp-host"

  /* Specify the address attributes for checks e.g. "ssh" or "http". */
  address = "$HOST_IP"

  /* Set custom attribute "os" for hostgroup assignment in "groups.conf". */
  vars.os = "$HOST_OS"
  vars.domain = "${HOST_NAME#*.}"

  vars.notification["mail"] = {
    /* The UserGroup "icingaadmins" is defined in "users.conf". */
    groups = [ "icingaadmins" ]
  }
}

// Service Declaration Block
// Service: SSH
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "ssh" {
    host_name     = "$HOST_NAME"
    check_command = "ssh"
}

// Service Declaration Block
// Service: Get hardware information
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_hardware" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_hardware"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
}


// Service Declaration Block
// Service: Get hardware information
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_software" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_software"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
}

// Service Declaration Block
// Service: Check cpu "busyness"
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_cpu" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_cpu"
    vars.cpu_warnlevel = 20
    vars.cpu_critlevel = 40
}

// Service Declaration Block
// Service: Check interface stats and throughput
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_ifstat" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_ifstat"
}
EOT
            continue
        fi

        #*************************************#
        # *** Define a Solaris/Linux host *** #
        #*************************************#
        echo "Trying to login as 'nms' to '$HOST_IP'"
        ssh-copy-id -i ${NMS_HOME}/.ssh/id_nms -o ConnectTimeout=120 nms@$HOST_IP
        if [ $? -ne 0 ]
        then
            cat << EOT >> /tmp/SSH_Hosts
Solaris/Linux host '$HOST_IP' doesn't seem to have the 'nms' account.

EOT
           continue
        fi
        scp -i ${NMS_HOME}/.ssh/id_nms -o ConnectTimeout=120 /tmp/$$.getinfo nms@$HOST_IP:/tmp/getinfo
        rm -f /tmp/$$.info
        ssh -i ${NMS_HOME}/.ssh/id_nms -o ConnectTimeout=120 nms@$HOST_IP "bash /tmp/getinfo" > /tmp/$$.info
        HOST_OS=$(awk -F= '/OS=/{print $NF}' /tmp/$$.info)

        # See https://zyradyl.github.io/2015/08/16/Icinga2-Tutorial-Part-2/
        cat << EOT > /etc/icinga2/conf.d/${HOST_NAME}.conf

object Host "$HOST_NAME" {
  display_name = "$HOST_NAME"
  /* Import the default host template defined in "templates.conf". */
  import "generic-host"
  import "unix-host"

  /* Specify the address attributes for checks e.g. "ssh" or "http". */
  address = "$HOST_IP"

  /* Set custom attribute "os" for hostgroup assignment in "groups.conf". */
  vars.os = "$HOST_OS"
  vars.domain = "${HOST_NAME#*.}"

  vars.notification["mail"] = {
    /* The UserGroup "icingaadmins" is defined in "users.conf". */
    groups = [ "icingaadmins" ]
  }
}

// Service Declaration Block
// Service: Get upload and download speeds
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_hardware" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_hardware"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
}

// Service Declaration Block
// Service: Get software information
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_software" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_software"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
}

// Service Declaration Block
// Service: Check partition space and inodes
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_disk" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_disk"
    vars.crps_warnlevel = 80
    vars.crps_critlevel = 90
}

// Service Declaration Block
// Service: Check cpu "busyness"
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_cpu" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_cpu"
    vars.cpu_warnlevel = 20
    vars.cpu_critlevel = 40
}

// Service Declaration Block
// Service: Check server load
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_load" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_load"
    vars.load_warnlevel = 15
    vars.load_critlevel = 30
}

// Service Declaration Block
// Service: Check server memory (and alert of swap space)
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_mem" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_mem"
    vars.swap_warnlevel = 50
    vars.swap_critlevel = 70
}

// Service Declaration Block
// Service: Check interface stats and throughput
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_check_ifstat" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_check_ifstat"
}
EOT

        # Run speed tests on primary mail servers
        [[ $HOST_NAME =~ ms1.* ]] && cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Get upload and download speeds
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "bluc_speedtest" {
    host_name     = "$HOST_NAME"
    check_command = "bluc_speedtest"
    // check every 12 hours (expressed in seconds)
    check_interval = 43200
}
EOT

        [ ! -z "$(grep ^HTTP= /tmp/$$.info)" ] && cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: HTTP
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "http" {
    host_name               = "$HOST_NAME"
    // Disable HTTPS checking
    vars.http_ssl           = "0"
    // Time till warning is issued
    vars.http_warn_time     = "5"
    // Time till critical notification
    vars.http_critical_time = "10"
    check_command           = "http"
}
EOT
        [ ! -z "$(grep ^HTTPS= /tmp/$$.info)" ] && cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: HTTP
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "https" {
    host_name               = "$HOST_NAME"
    // Enable HTTPS checking
    vars.http_ssl           = "1"
    // Time till warning is issued
    vars.http_warn_time     = "5"
    // Time till critical notification
    vars.http_critical_time = "10"
    check_command           = "http"
}

// Service Declaration Block
// Service: SSL Cert Check
// Note: The address specified in the above directive  
//       is carried into the service declaration                                                                                                                                                                          
//       blocks via specifying the host_name variable
//
object Service "ssl" {
    host_name                         = "$HOST_NAME"
    vars.ssl_port                     = "443"
    vars.ssl_cert_valid_days_warn     = "30"
    vars.ssl_cert_valid_days_critical = "15"
    check_command                     = "ssl"
}
EOT

        # Check all running processes for some other interesting stuff ...
        sed '1,/^All running processes:/d' /tmp/$$.info > /tmp/$$.procs
        if [ ! -z "$(grep dbsr[v] /tmp/$$.procs)" ]
        then
            # Check whether SyBase is running
            cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Check service/process status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_sybase" {
    host_name = "$HOST_NAME"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "dbsrv"
    vars.proc_min = 1
    vars.proc_max = 1
}
// Service Declaration Block
// Service: Check sybase status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_sybase" {
    host_name = "$HOST_NAME"
    check_command = "bluc_check_sybase"
}
EOT
        fi
        if [ ! -z "$(grep 'named.*bin[d]' /tmp/$$.procs)" ]
        then
            # Check whether Postfix is running
            cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Check service/process status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_named" {
    host_name               = "$HOST_NAME"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "named.*bin[d]"
    vars.proc_min = 1
    vars.proc_max = 2
}
EOT
        fi
        if [ ! -z "$(grep 'postfix.*maste[r]' /tmp/$$.procs)" ]
        then
            # Check whether Postfix is running
            cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Check service/process status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_postfix" {
    host_name               = "$HOST_NAME"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "postfix.*master"
    vars.proc_min = 1
    vars.proc_max = 2
}
EOT
        fi
        if [ ! -z "$(grep 'amavisd.*maste[r]' /tmp/$$.procs)" ]
        then
            # Check whether Postfix is running
            cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Check service/process status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_amavisd" {
    host_name               = "$HOST_NAME"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "amavisd.*master"
    vars.proc_min = 2
    vars.proc_min = 9999
}
EOT
        fi
        if [ ! -z "$(grep 'clamav.*clamd' /tmp/$$.procs)" ]
        then
            # Check whether clamd is running
            cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Check service/process status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_clamd" {
    host_name               = "$HOST_NAME"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "clamd"
    vars.proc_min = 1
    vars.proc_max = 2
}
EOT
        fi
        if [ ! -z "$(grep 'java.*tomca[t]' /tmp/$$.procs)" ]
        then
            # Check whether Tomcat is running
            cat << EOT >> /etc/icinga2/conf.d/${HOST_NAME}.conf

// Service Declaration Block
// Service: Check service/process status
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
object Service "bluc_check_process_tomcat" {
    host_name               = "$HOST_NAME"
    check_command = "bluc_check_process"
    // Add a max. CPU percentage after the process name, e.g. ":10"
    vars.proc_name = "tomcat"
    vars.proc_min = 1
    vars.proc_max = 1
}
EOT
        fi
    elif [ ! -z "$(grep 'Ports: 23/open' /tmp/$$.connectivity)" ]
    then
        if [[ $HOST_VENDOR =~ Cisco ]]
        then
            #*********************************#
            # *** Define a Cisco firewall *** #
            #*********************************#
            HOST_OS='Cisco'

            # See https://zyradyl.github.io/2015/08/16/Icinga2-Tutorial-Part-2/
            cat << EOT > /etc/icinga2/conf.d/${HOST_NAME}.conf

object Host "$HOST_NAME" {
  display_name = "$HOST_NAME"
  /* Import the default host template defined in "templates.conf". */
  import "generic-host"
  import "snmp-host"

  /* Specify the address attributes for checks e.g. "ssh" or "http". */
  address = "$HOST_IP"

  /* Set custom attribute "os" for hostgroup assignment in "groups.conf". */
  vars.os = "$HOST_OS"
  vars.domain = "${HOST_NAME#*.}"

  vars.notification["mail"] = {
    /* The UserGroup "icingaadmins" is defined in "users.conf". */
    groups = [ "icingaadmins" ]
  }
}

// Service Declaration Block
// Service: Telnet
// Note: The address specified in the above directive  
//       is carried into the service declaration 
//       blocks via specifying the host_name variable
//
/*
object Service "telnet" {
    host_name     = "$HOST_NAME"
    check_command = "telnet"
}
*/
EOT
            continue
        fi
    elif [ ! -z "$(grep 'Ports: 22/open' /tmp/$$.connectivity)" ]
    then
        #*******************************#
        # *** Define a Windows host *** #
        #*******************************#
        cat << EOT >> /tmp/Windows_Hosts
Windows host '$HOST_IP' doesn't seem to have the 'nms' account.

EOT
           continue
    fi

    RESTART_ICINGA2=1
done < $INPUT_LIST

# Commands are rebuilt by /usr/local/sbin/nms-checks.conf
# Groups are rebuilt by /usr/local/sbin/nms-checks.conf

if [ $RESTART_ICINGA2 -ne 0 ]
then
    icinga2 daemon -C &> /tmp/$$.conftest
    if [ $? -eq 0 ]
    then
        service icinga2 reload
    else
        cat /tmp/$$.conftest
    fi
fi

# Show the list of hosts which are reachable via SSH
cat << EOT
1. For newly discovered hosts, you need to setup the "nms" on it:
=> useradd -c 'NMS Monitoring' -m -s /bin/bash -G sudo nms  (Linux)
=> useradd -c 'NMS Monitoring' -m -s /usr/bin/bash nms  (Solaris)
=> passwd nms (use "b3kkajudd" as the password)

If you omit the next two steps, the remote checks will fail with an error
 message: Host key verification failed

2. Login in at least ONCE by hand like this:
sudo -u nagios ssh -i ${NMS_HOME}/.ssh/id_nms nms@<host>

3. Lastly, copy the ssh key to the new host:
a) via program
 ssh-copy-id -i ${NMS_HOME}/.ssh/id_nms -o ConnectTimeout=120 nms@<host>
b) manually
 sudo -u nagios scp ${NMS_HOME}/.ssh/id_nms.pub -o ConnectTimeout=120 nms@<host>:.ssh/authorized_keys
 sudo -u nagios ssh -i ${NMS_HOME}/.ssh/id_nms nms@<host> chmod 0600 .ssh/authorized_keys

EOT
[ -s /tmp/SSH_Hosts ] && (echo 'List of discovered SSH hosts:'; cat /tmp/SSH_Hosts)
[ -s /tmp/Windows_Hosts ] && (echo 'List of discovered Windows hosts:'; cat /tmp/Windows_Hosts)

# We are done
exit 0
